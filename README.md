# Lapres Praktikum Modul 1 Kelompok D07
Anggota Kelompok ''D07'' 
| Nama                      | NRP        |
|---------------------------|------------|
| Hanun Shaka Puspa         | 5025211051 |
| Mavaldi Rizqy Hazdi       | 5025211086 |
| Daffa Saskara             | 5025201249 |

## 1. Lossles.C
### Penjelasan Solusi
Pada program saya, terdapat source code huffman code dari internet dengan beberapa modifikasi sebagai berikut:
```c
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <string.h>
#include <ctype.h>

#define MAX_CHAR 256
#define MAX_TREE_HT 50

char send[26];

struct MinHeapNode
{
   char data;             // Karakter pada file
   unsigned freq;         // Frekuensi karakter
   struct MinHeapNode* left;
   struct MinHeapNode* right;
};

struct MinHeap {
  unsigned size;
  unsigned capacity;
  struct MinHeapNode **array;
};

struct MinHeapNode *newNode(char data, unsigned freq) {
  struct MinHeapNode *temp = (struct MinHeapNode *)malloc(sizeof(struct MinHeapNode));

  temp->left = temp->right = NULL;
  temp->data = data;
  temp->freq = freq;

  return temp;
}

struct MinHeap *createMinH(unsigned capacity) {
  struct MinHeap *minHeap = (struct MinHeap *)malloc(sizeof(struct MinHeap));

  minHeap->size = 0;

  minHeap->capacity = capacity;

  minHeap->array = (struct MinHeapNode **)malloc(minHeap->capacity * sizeof(struct MinHeapNode *));
  return minHeap;
}

void swapMinHeapNode(struct MinHeapNode **a, struct MinHeapNode **b) {
  struct MinHeapNode *t = *a;
  *a = *b;
  *b = t;
}

void minHeapify(struct MinHeap *minHeap, int idx) {
  int smallest = idx;
  int left = 2 * idx + 1;
  int right = 2 * idx + 2;

  if (left < minHeap->size && minHeap->array[left]->freq < minHeap->array[smallest]->freq)
    smallest = left;

  if (right < minHeap->size && minHeap->array[right]->freq < minHeap->array[smallest]->freq)
    smallest = right;

  if (smallest != idx) {
    swapMinHeapNode(&minHeap->array[smallest], &minHeap->array[idx]);
    minHeapify(minHeap, smallest);
  }
}

int checkSizeOne(struct MinHeap *minHeap) {
  return (minHeap->size == 1);
}

struct MinHeapNode *extractMin(struct MinHeap *minHeap) {
  struct MinHeapNode *temp = minHeap->array[0];
  minHeap->array[0] = minHeap->array[minHeap->size - 1];

  --minHeap->size;
  minHeapify(minHeap, 0);

  return temp;
}

void insertMinHeap(struct MinHeap *minHeap, struct MinHeapNode *minHeapNode) {
  ++minHeap->size;
  int i = minHeap->size - 1;

  while (i && minHeapNode->freq < minHeap->array[(i - 1) / 2]->freq) {
    minHeap->array[i] = minHeap->array[(i - 1) / 2];
    i = (i - 1) / 2;
  }
  minHeap->array[i] = minHeapNode;
}

void buildMinHeap(struct MinHeap *minHeap) {
  int n = minHeap->size - 1;
  int i;

  for (i = (n - 1) / 2; i >= 0; --i)
    minHeapify(minHeap, i);
}

int isLeaf(struct MinHeapNode *root) {
  return !(root->left) && !(root->right);
}

struct MinHeap *createAndBuildMinHeap(char data[], int freq[], int size) {
  struct MinHeap *minHeap = createMinH(size);

  for (int i = 0; i < size; ++i)
    minHeap->array[i] = newNode(data[i], freq[i]);

  minHeap->size = size;
  buildMinHeap(minHeap);

  return minHeap;
}

struct MinHeapNode *buildHuffmanTree(char data[], int freq[], int size) {
  struct MinHeapNode *left, *right, *top;
  struct MinHeap *minHeap = createAndBuildMinHeap(data, freq, size);

  while (!checkSizeOne(minHeap)) {
    left = extractMin(minHeap);
    right = extractMin(minHeap);

    top = newNode('$', left->freq + right->freq);

    top->left = left;
    top->right = right;

    insertMinHeap(minHeap, top);
  }
  return extractMin(minHeap);
}

void printArray(int arr[], int n, char c) {
    FILE *fp = fopen("temp.txt", "a");

    if (fp == NULL) {
        printf("Error opening file\n");
        return;
    }

  int i;

  fprintf(fp, "%c", c);
  for (i = 0; i < n; ++i){
    fprintf(fp, "%d", arr[i]);
  }

  fprintf(fp, "%c", '\n');
  fclose(fp);
}

void printHCodes(struct MinHeapNode *root, int arr[], int top) {
  if (root->left) {
    arr[top] = 0;
    printHCodes(root->left, arr, top + 1);
  }
  if (root->right) {
    arr[top] = 1;
    printHCodes(root->right, arr, top + 1);
  }
  if (isLeaf(root) && strchr(send, root->data)!=NULL) {
    printArray(arr, top, root->data);
  }
}

void HuffmanCodes(char data[], int freq[], int size) {
  struct MinHeapNode *root = buildHuffmanTree(data, freq, size);

  int arr[MAX_TREE_HT], top = 0;

  printHCodes(root, arr, top);
}
```

### **1a**
Pada parent process, baca file yang akan dikompresi dan hitung frekuensi kemunculan huruf pada file tersebut. Kirim hasil perhitungan frekuensi tiap huruf ke child process.

```c
else { 
        FILE *fp;
        char filename[100];
        char c;

        //file.txt dibuka
        fp = fopen("file.txt", "r");
        //jika terjadi error dalam pembukaan file
        if (fp == NULL) {
            printf("Error opening file\n");
            return 1;
        }

        int freq[26] = {0}; 
        char alpha[26] = {'\0'}; 
        int idx = 0; 
        //pembacaan setiap karakter dengan fungsi fgetc()
        while ((c = fgetc(fp)) != EOF) {
            //pengecekan karakter huruf dengan menggunakan isalpha()
            if (isalpha(c)) { 
                /*jika karakter termasuk huruf kecil, maka akan diubah menjadi huruf kapital 
                dengan fungsi toupper()*/
                c = toupper(c);
                /*perhitungan frekuensi karakter dengan menggunakan array freq dengan indeks 
                sesuai dengan posisi karakter dalam alfabet*/
                int pos = c - 'A'; 
                freq[pos]++;
                /*apabila karakter belum pernah muncul sebelumnya, maka karakter tersebut 
                akan ditambahkan ke array alpha dan indeks pada array alpha akan ditambahkan satu.*/ 
                if (freq[pos] == 1) { 
                    alpha[idx] = c; 
                    idx++; 
                }
            }
        }
        //array alpha dan freq akan disalin ke dua buah array baru yaitu arr dan arrfr
        char *arr = malloc(sizeof(char) * 26);
        int *arrfr = malloc(sizeof(int) * 26);

        //menyimpan huruf yang telah dibaca frekuensinya pada array send
        int j = 0;
        for (int i = 0; i < 26; i++) {
            alpha[i] = 'A' + i;
            arrfr[i] = freq[i];
      
            if(freq[i]!=0){
                send[j] = alpha[i];
                j++;
            }
        }

        ////array alpha yang berisi 26 huruf abjad akan disalin ke dalam array arr
        for (int i = 0; i < 26; i++) {
            arr[i] = alpha[i];
        }

        //menutup file.txt
        fclose(fp);

        //penutupan file descriptor 
        close(fd1[0]);
        /*Parent proses akan menuliskan data yang sudah dikomputasi ke pipe menggunakan write(). 
        Data yang akan dituliskan adalah array arr, array arrfr, dan array send*/
        write(fd1[1], arr, sizeof(char) * 26);
        write(fd1[1], arrfr, sizeof(int) * 26);
        write(fd1[1], send, sizeof(send));
        close(fd1[1]);

        //penutupan bagian write dari pipe menggunakan close() pada fd1[1]
        close(fd2[1]);
```
Pada parent proses ``else{}``, yaitu jika kondisi pid lebih dari 0 atau positif, maka akan dilakukan pembacaan file file.txt, kemudian membaca karakter dengan fungsi ``fgetc()`` dan ``isalpha()`` untuk membuktikan bahwa karakter merupakan huruf dan dilakukan perhitungan frekuensi menggunakan array ``freq``, dan yang terakhir yaitu mengirim hasil perhitungan frekuensi ke child proses dengan menggunakan pipe pertama.

### **1b**
Pada child process, lakukan kompresi file dengan menggunakan algoritma Huffman berdasarkan jumlah frekuensi setiap huruf yang telah diterima.

```c
if (pid == 0) { 
        /*Membuat tiga buah pointer yang akan digunakan untuk menyimpan array karakter, 
        array integer, dan string karakter.*/
        char *arr = malloc(sizeof(char) * 26);
        int *arrint = malloc(sizeof(int) * 26);
        char *sent = malloc(sizeof(char)*26);

        //menutup file descriptor
        close(fd1[1]);

        /*membaca tiga data dari pipe: array karakter (arr), array integer (arrint), 
        dan string karakter (sent)*/
        read(fd1[0], arr, sizeof(char) * 26);
        read(fd1[0], arrint, sizeof(int) * 26);
        read(fd1[0], sent, 26);

        //menutup file descriptor untuk membaca dari pipe (fd1[0])
        close(fd1[0]);

        //Mengcopy string yang berisi karakter yang akan dienkripsi ke variabel send
        strcpy(send, sent);

        //menutup file descriptor fd1[0] karena sudah tidak digunakan lagi
        close(fd1[0]);

        /*melakukan encoding terhadap array karakter (arr) dan array integer (arrint) 
        dengan ukuran data 26*/
        HuffmanCodes(arr, arrint, sizeof(char)*26);

        //menutup file descriptor fd2[0]
        close(fd2[0]);
}
```
Setelah hasil perhitungan frekuensi huruf dikirim dari parent proses, child proses akan membaca data melalui pipe yang dikirim menggunakan fungsi ``read()``, kemudian string akan di copy menggunakan ``strcpy()``, dan yang terakhir dilakukan encoding terhadap karakter dan integer dengan memanggil fungsi ``HuffmanCodes()``.

### **1c**
Kemudian (pada child process), simpan Huffman tree pada file terkompresi. Untuk setiap huruf pada file, ubah karakter tersebut menjadi kode Huffman dan kirimkan kode tersebut ke program dekompresi menggunakan pipe.
```c
        /*menghitung jumlah baris dalam file sementara temp.txt dengan memanggil fungsi 
        count_line dan menyimpan hasilnya di variabel countLine*/
        int countLine = count_line("temp.txt");

        //menuliskan jumlah baris ke dalam pipe writing end (fd2[1]) menggunakan fungsi write().
        write(fd2[1], &countLine, sizeof(countLine));
        //membuka file temp.txt untuk dibaca dan menyimpannya di pointer fp
        FILE *fp = fopen("temp.txt", "r");
            if(fp==NULL){
                perror("fail to open");
                exit(1);
            }
            char line[512];
            //Membaca isi dari file "temp.txt" baris per baris menggunakan fungsi fgets()
            while(fgets(line, 512, fp)!= NULL){
                /*Menuliskan isi dari setiap baris ke dalam pipe writing end fd2[1]
                menggunakan fungsi write()*/
                write(fd2[1], &line, sizeof(line));
            }
        //menutup file descriptor fd2[1]
        close(fd2[1]);
```
Untuk melakukan proses selanjutnya, dilakukan pemanggilan fungsi ``count_line`` yang berguna untuk menghitung jumlah baris dalam file sementara 'temp.txt', kemudian dilakukan pembacaan baris per baris menggunakan fungsi ``fgets()`` dan menuliskannya ke dalam pipe fd2[1] menggunakan fungsi ``write()``.
kode fungsi count_line:
```c
int count_line(char filename[]){
    int count = 0;
    char line[512];
    //membuka file
    FILE *fp = fopen(filename, "r");

    //jika file tidak dapat dibuka maka terjadi error
    if (fp == NULL) {
        printf("Error opening file.\n");
        return 1;
    }

    //membaca setiap baris dalam file menggunakan fungsi fgets()
    while (fgets(line, 512, fp) != NULL) {
        count++;
    }

    //menutup file
    fclose(fp);

    //return jumlah baris yang telah dihitung
    return count;
}
```
### **1d**
Kirim hasil kompresi ke parent process. Lalu, di parent process baca Huffman tree dari file terkompresi. Baca kode Huffman dan lakukan dekompresi.
```c
        //membaca integer n yang dikirimkan melalui pipe.
        int n;
        read(fd2[0], &n, sizeof(int));

        char coded[n][512];
        //membaca string-string yang terkompresi dalam array coded
        for (int i = 0; i < n; i++) {
            read(fd2[0], coded[i], sizeof(char) * 512);
        }
        //Parent proses akan menutup bagian read dari pipe menggunakan close() pada fd2[0]
        close(fd2[0]);
        int number = 0;
        // menghitung jumlah bit yang digunakan untuk merepresentasikan data
        for(int i = 0; i<26; i++){
            int calc;
            if(freq[i]!=0){
                //kompresi menjadi bit
                calc = freq[i]*8;
                //Jumlah bit ditambahkan ke variabel number
                number += calc;
            }
        }
```
Pada parent proses, dilakukan pembacaan data yang dikirim melalui pipe menggunakan fungsi ``read()`` dan dikompresi menjadi bit dengan mengalikan freq dengan 8 karakter, karena setiap karakter dalam ASCII memerlukan 8 bit untuk direpresentasikan.
### **1e**
Di parent process, hitung jumlah bit setelah dilakukan kompresi menggunakan algoritma Huffman dan tampilkan pada layar perbandingan jumlah bit antara setelah dan sebelum dikompres dengan algoritma Huffman.
```c
        printf("Jumlah bit SEBELUM dilakukan kompresi adalah %d\n", number);

        //menghitung jumlah bit setelah proses kompresi menggunakan fungsi translate()
        number = translate("translated.txt");
        printf("Jumlah bit SETELAH dilakukan kompresi adalah %d\n", number);

        //menghapus file "temp.txt"
        system("rm temp.txt");

```
Terakhir, dilakukan perbandingan jumlah bit dengan menggunakan fungsi ``translate()`` pada jumlah bit setelah dilakukan kompresi dengan huffman code.
kode fungsi translate:
```c
int translate(char filename[]) {
    /*membuka file tujuan (yang akan diisi hasil decode), 
    file temporary yang berisi encoding table, dan file yang akan di-decode*/
    FILE *fp = fopen(filename, "a");
    FILE *fs = fopen("temp.txt", "r");
    FILE *fc = fopen("file.txt", "r");
    
    //jika file-file gagal dibuka akan terjadi error
    if (fp == NULL || fs == NULL || fc == NULL) {
        printf("Error opening file.\n");
        return 1;
    }

    char line[512];
    char c;
    int count = 0;
    //Melakukan loop pada setiap karakter pada file yang akan di-decode
    while ((c = fgetc(fc)) != EOF) {
        /*fseek() digunakan untuk mengubah posisi pointer pada file encoding table menjadi awal, 
        yaitu pada karakter pertama*/
        fseek(fs, 0, SEEK_SET);

        /*toupper() digunakan untuk mengubah karakter pada file yang akan di-decode 
        menjadi huruf kapital*/
        c = toupper(c);

        while (fgets(line, 512, fs) != NULL) {
            /*cek apakah karakter pertama pada suatu baris sama dengan 
            karakter pada file yang akan di-decode*/

            if (line[0] == c) {
                //memindahkan isi dari baris tersebut ke variabel sub
                char *sub = malloc(strlen(line));
                strcpy(sub, line + 1);

                //mengecek panjang sub dan menambahkan jumlah karakter yang didecode ke variabel count
                count += strlen(sub)-1;

                /*karakter terakhir pada sub akan dihapus karena 
                merupakan karakter newline pada file encoding table*/
                sub[strlen(sub) - 1] = '\0';

                //Hasil decode pada sub akan ditulis pada file tujuan menggunakan fwrite()
                fwrite(sub, strlen(sub), 1, fp);

                //memori yang digunakan untuk sub akan dilepaskan dengan free()
                free(sub);
                //looping terhenti
                break;
            }
        }
    }

    //Menutup ketiga file yang telah dibuka
    fclose(fp);
    fclose(fs);
    fclose(fc);

    //mengembalikan jumlah karakter yang berhasil didecode
    return count;
}
```

### Screenshot Program

![ss_modul_3](/uploads/59ad5357d67843f90bb073ee8528d5ba/ss_modul_3.png)

## 2. Soal 2
Pada soal No.2 ini akan ada 3 program `.c` yang akan digunakan yaitu `kalian.c`, `cinta.c`, dan `sisop.c`.
### Penjelasan Solusi
#### **`kalian.c`**
Program C ini melakukan perkalian matriks antara dua matriks dengan ukuran yang berbeda, lalu menyimpan hasilnya dalam shared memory.
```sh
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/ipc.h>
#include <sys/shm.h>

#define ROW1 4
#define COL1 2
#define ROW2 2
#define COL2 5

int main() {
    srand(time(NULL));

    int matrix1[ROW1][COL1];
    int matrix2[ROW2][COL2];
    int result[ROW1][COL2];

    // Fill matrix 1 with random numbers from 1 to 5
    for (int i = 0; i < ROW1; i++) {
        for (int j = 0; j < COL1; j++) {
            matrix1[i][j] = rand() % 5 + 1;
        }
    }
    
    printf("Matrix 1:\n");
    for (int i = 0; i < ROW1; i++) {
        for (int j = 0; j < COL1; j++) {
            printf("%d ", matrix1[i][j]);
        }
        printf("\n");
    }

    // Fill matrix 2 with random numbers from 1 to 4
    for (int i = 0; i < ROW2; i++) {
        for (int j = 0; j < COL2; j++) {
            matrix2[i][j] = rand() % 4 + 1;
        }
    }
    
    printf("\nMatrix 2:\n");
    for (int i = 0; i < ROW2; i++) {
        for (int j = 0; j < COL2; j++) {
            printf("%d ", matrix2[i][j]);
        }
        printf("\n");
    }

    // Matrix multiplication
    for (int i = 0; i < ROW1; i++) {
        for (int j = 0; j < COL2; j++) {
            result[i][j] = 0;
            for (int k = 0; k < COL1; k++) {
                result[i][j] += matrix1[i][k] * matrix2[k][j];
            }
        }
    }
    
    printf("\nResult:\n");
    for (int i = 0; i < ROW1; i++) {
        for (int j = 0; j < COL2; j++) {
            printf("%d ", result[i][j]);
        }
        printf("\n");
    }

    // Create shared memory
    key_t key = ftok("shmfile", 65);
    int shmid = shmget(key, sizeof(result), 0666 | IPC_CREAT);
    int (*shared_result)[COL2] = shmat(shmid, NULL, 0);

    // Copy the result to shared memory
    for (int i = 0; i < ROW1; i++) {
        for (int j = 0; j < COL2; j++) {
            shared_result[i][j] = result[i][j];
        }
    }

    // Detach shared memory
    shmdt(shared_result);

    return 0;
}
```
Berikut adalah penjelasan dari kode-kode dalam `kalian.c`:

```sh
#define ROW1 4
#define COL1 2
#define ROW2 2
#define COL2 5
```
Bagian diatas berguna untuk mendefinisikan ukuran matriks yaitu matriks 1 `ROW1xCOL1` -> `4x2` dan matriks 2 `ROW2xCOL2` -> `2x5`.

```sh
srand(time(NULL));
```
Fungsi ini akan melakukan inisiasi agar fungsi `rand()` bisa dipakai.

```sh
    int matrix1[ROW1][COL1];
    int matrix2[ROW2][COL2];
    int result[ROW1][COL2];
```
Bagian ini berisi pembuatan array integer 2 dimensi untuk matriks1, matriks2, dan matriks hasil perkalian.

```sh
// Fill matrix 1 with random numbers from 1 to 5
    for (int i = 0; i < ROW1; i++) {
        for (int j = 0; j < COL1; j++) {
            matrix1[i][j] = rand() % 5 + 1;
        }
    }
    
    printf("Matrix 1:\n");
    for (int i = 0; i < ROW1; i++) {
        for (int j = 0; j < COL1; j++) {
            printf("%d ", matrix1[i][j]);
        }
        printf("\n");
    }
```
Bagian ini akan mengisi matriks pertama dengan angka random dari 1 sampai 5 menggunakan fungsi `rand()`. Lalu menampilkan elemen-elemen dari matriks pertama.

```sh
// Fill matrix 2 with random numbers from 1 to 4
    for (int i = 0; i < ROW2; i++) {
        for (int j = 0; j < COL2; j++) {
            matrix2[i][j] = rand() % 4 + 1;
        }
    }
    
    printf("\nMatrix 2:\n");
    for (int i = 0; i < ROW2; i++) {
        for (int j = 0; j < COL2; j++) {
            printf("%d ", matrix2[i][j]);
        }
        printf("\n");
    }
```
Bagian ini akan mengisi matriks kedua dengan angka random dari 1 sampai 4 menggunakan fungsi `rand()`. Lalu menampilkan elemen-elemen dari matriks kedua.

```sh
// Matrix multiplication
    for (int i = 0; i < ROW1; i++) {
        for (int j = 0; j < COL2; j++) {
            result[i][j] = 0;
            for (int k = 0; k < COL1; k++) {
                result[i][j] += matrix1[i][k] * matrix2[k][j];
            }
        }
    }
    
    printf("\nResult:\n");
    for (int i = 0; i < ROW1; i++) {
        for (int j = 0; j < COL2; j++) {
            printf("%d ", result[i][j]);
        }
        printf("\n");
    }
```
Bagian ini akan melakukan perkalian matriks pertama dan matriks kedua lalu menyimpannya ke matriks `result`, matriks `result` berukuran `4x5` karena jika dilakukan perkalian matriks antara matriks pertama dan matriks kedua akan menghasilkan matriks berukuran `4x5`. Lalu menampilkan hasil perkalian dari matriks pertama dan matriks kedua (matriks `result`).

```sh
// Create shared memory
    key_t key = ftok("shmfile", 65);
    int shmid = shmget(key, sizeof(result), 0666 | IPC_CREAT);
    int (*shared_result)[COL2] = shmat(shmid, NULL, 0);

    // Copy the result to shared memory
    for (int i = 0; i < ROW1; i++) {
        for (int j = 0; j < COL2; j++) {
            shared_result[i][j] = result[i][j];
        }
    }

    // Detach shared memory
    shmdt(shared_result);
```
Bagian ini akan membuat shared memory:

- Menggunakan `ftok()` untuk menghasilkan kunci unik
- Menggunakan `shmget()` untuk membuat segmen shared memory dengan ukuran yang sama dengan matriks result
- Menggunakan `shmat()` untuk menghubungkan segmen shared memory ke alamat memori program
- Menyalin hasil perkalian matriks ke shared memory sebagai `shared_result[][]`.
- Melepaskan shared memory dengan `shmdt()`.

#### **`cinta.c`**
Secara garis besar program ini akan menampilkan hasil perkalian matriks dari `kalian.c` menggunakan shared memory dan membuat factorial dari setiap elemen menggunakan *thread dan multithreading*.
```sh
#include <stdio.h>
#include <pthread.h>
#include <sys/ipc.h>
#include <sys/shm.h>
//#include <time.h>

#define ROWS 4
#define COLS 5

typedef struct {
    int row;
    int col;
    int value;
} MatrixElement;

unsigned long long factorial_matrix[ROWS][COLS];

void *factorial(void *arg) {
    MatrixElement *element = (MatrixElement *)arg;
    unsigned long long fact = 1;

    for (int i = 1; i <= element->value; i++) {
        fact *= i;
    }

    factorial_matrix[element->row][element->col] = fact;
    return NULL;
}

int main() {

    /*
    clock_t start, end;
    start = clock();
    */
    
    // Access shared memory
    key_t key = ftok("shmfile", 65);
    int shmid = shmget(key, sizeof(int[ROWS][COLS]), 0666 | IPC_CREAT);
    int (*result)[COLS] = shmat(shmid, NULL, 0);

    // Print the result matrix
    printf("Result matrix:\n");
    for (int i = 0; i < ROWS; i++) {
        for (int j = 0; j < COLS; j++) {
            printf("%d ", result[i][j]);
        }
        printf("\n");
    }

    // Calculate factorials using multithreading
    pthread_t threads[ROWS][COLS];
    MatrixElement elements[ROWS][COLS];

    for (int i = 0; i < ROWS; i++) {
        for (int j = 0; j < COLS; j++) {
            elements[i][j].row = i;
            elements[i][j].col = j;
            elements[i][j].value = result[i][j];
            pthread_create(&threads[i][j], NULL, factorial, &elements[i][j]);
        }
    }

    for (int i = 0; i < ROWS; i++) {
        for (int j = 0; j < COLS; j++) {
            pthread_join(threads[i][j], NULL);
        }
    }

    // Print the factorial matrix
    printf("\nFactorial matrix:\n");
    for (int i = 0; i < ROWS; i++) {
        for (int j = 0; j < COLS; j++) {
            printf("%llu ", factorial_matrix[i][j]);
        }
        printf("\n");
    }

    // Detach and destroy shared memory
    shmdt(result);
    shmctl(shmid, IPC_RMID, NULL);
    
    /*
    end = clock();
    double duration = ((double)end - start)/CLOCKS_PER_SEC;
    printf("Time taken to execute in seconds : %f \n", duration);
    */

    return 0;
}
```
Berikut adalah penjelasan dari kode-kode yang terdapat dalam `cinta.c`:
```sh
#define ROWS 4
#define COLS 5

typedef struct {
    int row;
    int col;
    int value;
} MatrixElement;

unsigned long long factorial_matrix[ROWS][COLS];
```
Bagian diatas membuat struct agar saat mengambil elemen matriks akan lebih mudah pada thread dan fungsi `factorial()` nantinya. Lalu membuat array 2 dimensi dengan ukuran `4x5` yang akan digunakan untuk tempat menyimpan factorial dari elemen-elemen matriks.

```sh
void *factorial(void *arg) {
    MatrixElement *element = (MatrixElement *)arg;
    unsigned long long fact = 1;

    for (int i = 1; i <= element->value; i++) {
        fact *= i;
    }

    factorial_matrix[element->row][element->col] = fact;
    return NULL;
}
```
Fungsi ini akan digunakan untuk menghitung factorial dari elemen-elemen matriks dengan cara mengambil nilai elemen matriks dari struct `MatrixElement` lalu menghitung factorial dari elemen tersebut dan disimpan ke `factorial_matrix[][]` sesuai dengan indeks baris dan kolomnya.

```sh
// Access shared memory
    key_t key = ftok("shmfile", 65);
    int shmid = shmget(key, sizeof(int[ROWS][COLS]), 0666 | IPC_CREAT);
    int (*result)[COLS] = shmat(shmid, NULL, 0);

    // Print the result matrix
    printf("Result matrix:\n");
    for (int i = 0; i < ROWS; i++) {
        for (int j = 0; j < COLS; j++) {
            printf("%d ", result[i][j]);
        }
        printf("\n");
    }
```
Bagian ini akan melakukan akses ke shared memory untuk mengambil hasil perkalian matriks dari `kalian.c` lalu menampilkan matriks tersebut.

```sh
// Calculate factorials using multithreading
    pthread_t threads[ROWS][COLS];
    MatrixElement elements[ROWS][COLS];

    for (int i = 0; i < ROWS; i++) {
        for (int j = 0; j < COLS; j++) {
            elements[i][j].row = i;
            elements[i][j].col = j;
            elements[i][j].value = result[i][j];
            pthread_create(&threads[i][j], NULL, factorial, &elements[i][j]);
        }
    }
```
Bagian ini akan menginisiasi thread sebanyak `4x5` sesuai ukuran matriks dan membuat struct yang nanti akan digunakan pada fungsi `factorial()`. Pada bagian loop akan dibuat atau mengisi thread menggunakan fungsi `pthread_create()` yang didalamnya akan dilakukan penghitungan factorial dari elemen-elemen matriks.

```sh
for (int i = 0; i < ROWS; i++) {
        for (int j = 0; j < COLS; j++) {
            pthread_join(threads[i][j], NULL);
        }
    }
```
Bagian ini akan melakukan join thread untuk melakukan penggabungan dengan thread lain yang telah berhenti (terminated).

```sh
// Print the factorial matrix
    printf("\nFactorial matrix:\n");
    for (int i = 0; i < ROWS; i++) {
        for (int j = 0; j < COLS; j++) {
            printf("%llu ", factorial_matrix[i][j]);
        }
        printf("\n");
    }
```
Bagian ini akan menampilkan hasil factorial dari semua elemen matriks dalam format matriks.

```sh
shmdt(result);
shmctl(shmid, IPC_RMID, NULL);
```
Bagian ini `shmdt()` akan melepaskan (detach) shared memory dan menghapusnya menggunakan fungsi `shmctl()`.

#### **`sisop.c`**
Program ini akan melakukan hal yang sama persis dengan `cinta.c` tanpa menggunakan *thread dan multithreading* untuk menghitung factorial dari setiap elemen matriks.

```sh
#include <stdio.h>
#include <sys/ipc.h>
#include <sys/shm.h>
//#include <time.h>

#define ROWS 4
#define COLS 5

unsigned long long factorial(int n) {
    unsigned long long fact = 1;
    for (int i = 1; i <= n; i++) {
        fact *= i;
    }
    return fact;
}

int main() {

    /*
    clock_t start, end;
    start = clock();
    */
    
    // Access shared memory
    key_t key = ftok("shmfile", 65);
    int shmid = shmget(key, sizeof(int[ROWS][COLS]), 0666 | IPC_CREAT);
    int (*result)[COLS] = shmat(shmid, NULL, 0);

    // Print the result matrix
    printf("Result matrix:\n");
    for (int i = 0; i < ROWS; i++) {
        for (int j = 0; j < COLS; j++) {
            printf("%d ", result[i][j]);
        }
        printf("\n");
    }

    // Calculate factorials and store them in a matrix
    unsigned long long factorial_matrix[ROWS][COLS];
    for (int i = 0; i < ROWS; i++) {
        for (int j = 0; j < COLS; j++) {
            factorial_matrix[i][j] = factorial(result[i][j]);
        }
    }

    // Print the factorial matrix
    printf("\nFactorial matrix:\n");
    for (int i = 0; i < ROWS; i++) {
        for (int j = 0; j < COLS; j++) {
            printf("%llu ", factorial_matrix[i][j]);
        }
        printf("\n");
    }

    // Detach and destroy shared memory
    shmdt(result);
    shmctl(shmid, IPC_RMID, NULL);
    
    /*
    end = clock();
    double duration = ((double)end - start)/CLOCKS_PER_SEC;
    printf("Time taken to execute in seconds : %f \n", duration);
    */

    return 0;
}
```
Penjelasan dari bagian-bagian program ini sama dengan `cinta.c` yang berbeda yaitu pada fungsi `factorial()` yang akan langsung melakukan factorial dan me-return nilai factorial tanpa langsung memasukkannya kedalam array matriks. Perbedaan lainnya adalah tidak menggunakan struct dan tanpa *thread dan multithreading*.


**Untuk melakukan run soal no.2 diperlukan urutan:
`kalian.c` - `cinta.c` dan `kalian.c` - `sisop.c`. Jika ingin memakai hasil matriks dari `kalian.c` yang sama maka hapus bagian `shmctl(shmid, IPC_RMID, NULL);` dari `cinta.c` atau `sisop.c` tergantung program mana yang dijalankan duluan agar shared memory dari `kalian.c` tidak terhapus.**
### Screenshoot Solusi Soal 2
- Output dari program `kalian.c` pada terminal
![1](https://github.com/mavaldi/image-placeeee/blob/main/2%20-%201.png?raw=true)
- Output dari program `cinta.c` pada terminal
![2](https://github.com/mavaldi/image-placeeee/blob/main/2%20-%202.png?raw=true)
- Output dari program `sisop.c` pada terminal
![3](https://github.com/mavaldi/image-placeeee/blob/main/2%20-%203.png?raw=true)

### Kendala Pengerjaan Soal 2
Kendala pengerjaan pada soal no.2 adalah hasil dari factorial matriks terlalu besar, tetapi setelah batas nilai elemen matriks diganti hasil factorial dapat dihitung.
## 3. Playlist
### Penjelasan Solusi
Pada soal diminta membuat 2 program yang saling berkomunikasi menggunakan message queue. Program pertama, ```user.c``` merupakan sender dari message queue.

#### user.c

```
#define MAX_MSG_SIZE 1024
#define MAX_USERS 2

typedef struct msg_buffer {
    long msg_type;
    char msg_text[MAX_MSG_SIZE];
} MsgBuffer;

sem_t semaphore;
```

Didefinisikan variabel global size maksimal message sebagai 1024, user maksimum untuk semaphore sebanyak 2, dan struct ```msg_buffer``` sebagai buffer message yang akan dikirim.

```
void send_message() {
    // Send a message to the message queue
    key_t key = ftok("msgq_file", 'A');
    int msg_id = msgget(key, 0666 | IPC_CREAT);
    MsgBuffer msg_buffer;
    msg_buffer.msg_type = 1;

    while (1) {
        fgets(msg_buffer.msg_text, MAX_MSG_SIZE, stdin);
        msgsnd(msg_id, &msg_buffer, sizeof(MsgBuffer), 0);
        if (strcmp(msg_buffer.msg_text, "quit\n") == 0) {
            break;
        }
    }

    printf("Message queue deleted.\n");
}
```

Lalu, didefinisikan function ```send_message()``` yang akan berfungsi untuk menerima pesan dari konsol lalu mengirimkannya ke message queue. Jika user menginput "quit", maka program akan keluar dari loop dan terminate.

```
int main(int argc, char** argv) {
    sem_init(&semaphore, 0, 2);

    while (1) {
        if (sem_wait(&semaphore) == 0) {
            printf("Semaphore acquired!\n");

            send_message();

            sem_post(&semaphore);
            printf("Semaphore released.\n");
        } else {
            char msg[MAX_MSG_SIZE];
            fgets(msg, MAX_MSG_SIZE, stdin);
            printf("STREAM SYSTEM OVERLOAD\n");
        }
    }

    sem_destroy(&semaphore);
    return 0;
}
```

Selanjutnya pada main program, diinisialisasi semaphore sebanyak jumlah maksimal user. Lalu dilakukan perulangan. Jika semaphore belum didapatkan, program akan terus menunggu hingga semaphore tersedia. Sebaliknya jika semaphore telah didapatkan, maka fungsi ```send_message()``` akan dipanggil.

#### stream.c

```
#define MAX_MSG_SIZE 1024
#define MAX_USERS 2

typedef struct msg_buffer
{
    long msg_type;
    char msg_text[MAX_MSG_SIZE];
} MsgBuffer;

sem_t semaphore;
```

Sama seperti di ```user.c```, didefinisikan konstanta, struct message, dan semaphore yang akan digunakan.

```
void* receive_messages(void* arg) {
    key_t key = ftok("msgq_file", 'A');
    int msg_id = msgget(key, 0666 | IPC_CREAT);
    MsgBuffer msg_buffer;

    while (1) {
        ...
    }

    ...
}
```

Selanjutnya didefinisikan fungsi ```receive_message```. Di dalamnya, diinisialisasi key untuk id message dan buffer message. Selanjutnya program masuk ke dalam while loop.

```
    ...
    msgrcv(msg_id, &msg_buffer, sizeof(MsgBuffer), 1, 0);
        if (strstr(msg_buffer.msg_text, "quit") != NULL || strstr(msg_buffer.msg_text, "exit") != NULL) {
            break;
        }
    ...
```

Dipanggil ```msgrcv``` untuk menerima message dan ditampung dalam buffer message.

```
    ...
    if (strstr(msg_buffer.msg_text, "decrypt") != NULL || strstr(msg_buffer.msg_text, "DECRYPT") != 0) {
            printf("Decrypting...\n");

            // Create new txt file
            system("touch playlist.txt");

            // Clear the txt file content
            FILE *file;
            file = fopen("/home/nuna/sisop-praktikum-modul-3-2023-bs-d07/soal3/playlist.txt", "w");
            fclose(file);

            // Parse JSON file that contains array of objects
            json_t *root;
            json_error_t error;
            root = json_load_file("/home/nuna/sisop-praktikum-modul-3-2023-bs-d07/soal3/song-playlist.json", 0, &error);

            // Handle error
            if (!root) {
                fprintf(stderr, "Error parsing JSON: %s\n", error.text);
                exit(1);
            }

            size_t array_size = json_array_size(root);
            // Loop through array in JSON file
            for (size_t i = 0; i < array_size; i++) {
                json_t *object = json_array_get(root, i);
                if (!json_is_object(object))
                {
                    fprintf(stderr, "Error: JSON array element %zu is not an object\n", i);
                    json_decref(root);
                    exit(1);
                }

                json_t *method = json_object_get(object, "method");
                json_t *song = json_object_get(object, "song");

                char *strmethod = json_string_value(method);
                char *strsong = json_string_value(song);
                char result[1024];

                // Decrypt according to method
                if (strcmp(strmethod, "rot13") == 0)
                {
                    int length = strlen(strsong);
                    int j;
                    for (j = 0; j < length; j++)
                    {
                        if (strsong[j] >= 'a' && strsong[j] <= 'z') {
                            strsong[j] = 'a' + (strsong[j] - 'a' + 13) % 26;
                        } else if (strsong[j] >= 'A' && strsong[j] <= 'Z') {
                            strsong[j] = 'A' + (strsong[j] - 'A' + 13) % 26;
                        }
                    }
                    strcpy(result, strsong);
                } else if (strcmp(strmethod, "base64") == 0) {
                    size_t input_len = strlen(strsong);
                    size_t output_len = input_len * 3 / 4; // approximate output buffer size
                    unsigned char output[output_len];
                    memset(output, 0, output_len);

                    BIO *bio, *b64;
                    b64 = BIO_new(BIO_f_base64());
                    bio = BIO_new_mem_buf(strsong, input_len);
                    bio = BIO_push(b64, bio);
                    BIO_set_flags(bio, BIO_FLAGS_BASE64_NO_NL);
                    output_len = BIO_read(bio, output, input_len);

                    strcpy(result, output);

                    BIO_free_all(bio);
                } else {
                    char hex_str[1024];
                    strcpy(hex_str, strsong);
                    int len = strlen(hex_str) / 2; // Calculate the length of the binary string

                    // Allocate memory for the binary string
                    char *bin_str = (char *)malloc(len + 1);

                    // Convert the hexadecimal string to binary
                    for (int j = 0; j < len; j++) {
                        sscanf(hex_str + 2 * j, "%2hhx", bin_str + j);
                    }

                    // Null-terminate the binary string
                    bin_str[len] = '\0';

                    // Interpret the binary string as a character string
                    printf("Decrypted string: %s\n", bin_str);
                    strcpy(result, bin_str);

                    // Free the memory
                    free(bin_str);
                }

                FILE *file;
                file = fopen("/home/nuna/sisop-praktikum-modul-3-2023-bs-d07/soal3/playlist.txt", "a");

                // Store decrypted song to txt file
                fprintf(file, "%s\n", result);
                fclose(file);
            }

            // Clean JSON resources
            json_decref(root);
            printf("Decrypting done.\n");

            // Sort the lines in txt file alphabetically
            printf("Sorting...\n");
            system("sort -o playlist.txt playlist.txt");
            printf("Sorting done.\n============\n\n");
        }
    ...
```

Selanjutnya adalah pengecekan command yang dikirimkan dalam message. Jika command yang dikirimkan adalah "DECRYPT", maka program akan melakukan langkah-langkah berikut:
1. Membuat file ```playlist.txt``` menggunakan system call.
2. Memastikan isi file playlist kosong dengan membukan file pada mode write dan menutupnya lagi.
3. Load json file menggunakan library jansson.
4. Dapatkan size array of object pada file json lalu lakukan looping sebanyak size tersebut.
5. Untuk tiap perulangan, diambil satu object sesuai indeks arraynya, lalu diambil method dan song-nya sebagai string value. Kemudian, string song akan didekripsi sesuai method. Method dekripsi dapat berupa rotate13, base64, dan hex.
6. Hasil dekripsi tiap song akan diappend ke ```playlist.txt```
7. Setelah perulangan selesai, semua resource json dibersihkan dan file playlist ditutup.
8. Sesuai perrmintaan di soal, dipanggil sort dengan system call untuk mengurutkan isi playlist secara alfabetis.

```
    ...
    } else if (strstr(msg_buffer.msg_text, "list") != NULL || strstr(msg_buffer.msg_text, "LIST") != 0) {
            // Print the content of txt file
            printf("============\n");
            system("cat playlist.txt");
            printf("============\n\n");
        }
    }
    ...
```

Jika command berisi "LIST", maka akan ditampilkan semua isi playlist menggunakan system call ```cat```.

```
    ...
    } else if (strstr(msg_buffer.msg_text, "play") != NULL || strstr(msg_buffer.msg_text, "PLAY") != 0) {
            char song[MAX_MSG_SIZE];
            strcpy(song, msg_buffer.msg_text);
            
            // Remove 'play ' from the string
            memmove(song, song + 5, strlen(song));

            // Remove '\n' from the string
            song[strcspn(song, "\n")] = 0;

            // Open playlist file
            FILE *file;
            file = fopen("/home/nuna/sisop-praktikum-modul-3-2023-bs-d07/soal3/playlist.txt", "r");

            // Loop through and find the song and store it in array
            char songs_found[MAX_MSG_SIZE][MAX_MSG_SIZE];
            char line[1024];
            int j = 0;

            while (fgets(line, sizeof(line), file)) {
                // Uppercase letters in line
                for (int i = 0; line[i] != '\0'; i++) {
                    if (line[i] >= 'a' && line[i] <= 'z')
                    {
                        line[i] = line[i] - 32;
                    }
                }

                // Uppercase letters in song
                for (int i = 0; song[i] != '\0'; i++) {
                    if (song[i] >= 'a' && song[i] <= 'z')
                    {
                        song[i] = song[i] - 32;
                    }
                }

                if (strstr(line, song) != NULL) {
                    strcpy(songs_found[j], line);
                    j++;
                }
            }

            int sem_id = semget(key, 1, 0666 | IPC_CREAT);

            // Play the song
            if (j == 1) {
                // Remove newline character
                songs_found[0][strcspn(songs_found[0], "\n")] = 0;
                printf("USER %d PLAYING \"%s\"\n", sem_id, songs_found[0]);
                printf("============\n\n");
            } else if (j > 1) {
                printf("THERE ARE \"%d\" SONG CONTAINING \"%s\"\n", j, song);
                int k = 0;
                while (k < j) {
                    printf("%d. %s", k+1, songs_found[k]);
                    k++;
                }
            } else {
                printf("THERE IS NO SONG CONTAINING %s\n", song);
            }

            fclose(file);
            printf("===========================\n");
        }
        ...

```

Jika command berisi ```play```, maka dicopy command tersebut dan cukup diambil judul lagu yang ingin di-play dengan cara dihilangkan "play " dan newline "\n" di akhir command. Selanjutnya dibuka file playlist dengan mode read, lalu di-iterasi dari baris awal sampai akhir. Untuk tiap iterasi, jika ditemukan baris yang mengandung string lagu, baris tersebut disimpan dalam string ```songs_found```. Selanjutnya isi dari array tersebut di-outputkan sesuai format yang diminta di soal. Terakhir, file playlist ditutup.

```
        ...
        } else if (strstr(msg_buffer.msg_text, "add") != NULL || strstr(msg_buffer.msg_text, "ADD") != 0) {
            // Get song name
            char song[MAX_MSG_SIZE];
            strcpy(song, msg_buffer.msg_text);

            // Remove 'add ' from the string
            memmove(song, song + 4, strlen(song));

            // Remove trailing newline
            song[strcspn(song, "\n")] = 0;

            // Check if song already exist in file
            int already_exist = 0;
            FILE *file;
            file = fopen("/home/nuna/sisop-praktikum-modul-3-2023-bs-d07/soal3/playlist.txt", "r");
            while(fgets(msg_buffer.msg_text, sizeof(msg_buffer.msg_text), file) != NULL) {
                if (strstr(msg_buffer.msg_text, song) != NULL) {
                    printf("SONG ALREADY ON PLAYLIST\n");
                    already_exist = 1;
                }
            }

            if (!already_exist) {
                // Open playlist file in append mode
                FILE *file;
                file = fopen("/home/nuna/sisop-praktikum-modul-3-2023-bs-d07/soal3/playlist.txt", "a");

                // Write song into playlist file
                fprintf(file, "%s\n", song);
                fclose(file);

                // Sort the lines in txt file alphabetically
                system("sort -o playlist.txt playlist.txt");
                int sem_id = semget(key, 1, 0666 | IPC_CREAT);
                printf("USER %d ADD SONG \"%s\"\n", sem_id, song);
            }
        }
        ...
```

Terakhir, ada command ```add```. Mirip seperti command play, command ini hanya diambil judul lagunya. Kemudian, dilakukan iterasi pada playlist untuk mengecek apakah lagu sudah ada. Jika belum, maka lagu dari user di append, kemudian diurutkan lagi menggunakan system call.

```
    ...
        } else {
            printf("UNKNOWN COMMAND\n");
        }
    ...
```

Lalu jika ada command yang tidak sesuai, akan di-output "UNKNOWN COMMAND".

Pada main program, fungsi di atas dipanggil dengan membuat thread baru sebagai berikut.

```
pthread_create(&thread_id, NULL, receive_messages, NULL);
```

### Screenshoot Solusi Soal 3

<img src="https://i.ibb.co/vHrqQvd/Screenshot-from-2023-05-13-19-50-32.png" width=750>

<img src="https://i.ibb.co/tsPnPVs/Screenshot-from-2023-05-13-19-50-50.png" width=750>

### Kendala Pengerjaan Soal 3
Pada saat demo terdapat kesalahan saat meng-compile program. Ada beberapa library yang dibutuhkan, sehingga kompilasi untuk ```stream.c``` adalah:
```
gcc stream.c -lssl -lcrypto -pthread -ljansson -o stream
```
Dan untuk ```user.c``` :
```
gcc user.c -pthread -o user
```

Untuk semaphore, id semaphore yang digunakan saat mengeksekusi 2 program masih sama sehingga user tidak terlimit.
## 4. Soal 4
Pada No.4 ini ada 3 program ```.c``` yang akan dibutuhkan untuk penyelesaian soal ini.
### Penjelasan Solusi
#### **`unzip.c`**
```sh
#include <stdlib.h>
#include <stdio.h>

int main(void)
{
    //download file
    system("wget --no-check-certificate 'https://docs.google.com/uc?export=download&id=1rsR6jTBss1dJh2jdEKKeUyTtTRi_0fqp' -O hehe.zip");
    	
    //unzip file
    system("unzip hehe.zip");

    //remove file
    system("rm -rf hehe.zip");
    return 0;
}
```
Program pertama dalam soal adalah unzip.c, pada kode ini akan dilakukan:

1. Download file dari sebuah link menggunakan command wget dan memakai fungsi system.
```sh
system("wget --no-check-certificate 'https://docs.google.com/uc?export=download&id=1rsR6jTBss1dJh2jdEKKeUyTtTRi_0fqp' -O hehe.zip");
```
2. Unzip file ```hehe.zip```
```sh
system("unzip hehe.zip");
```
3. Remove file atau menghapus file yang telah di unzip
```sh
system("rm -rf hehe.zip");
```

#### **`categorize.c`**
```sh
#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>
#include <pthread.h>
#include <ctype.h>
#include <math.h> 

#define MAX_LEN 256
#define MAX_PATH 256
#define MAX_EXT 5
#define MAX_DEST_SIZE 475 //buffer max

pthread_mutex_t lock;
FILE* log_file;

struct ThreadArgs {
    char dirname[MAX_LEN];
    int maxFile;
};

void writeLogMade(char createdDir[]){
    // write log
    time_t now = time(NULL);
    struct tm *t = localtime(&now);
    char logMADE[256] = "MADE ";
    char _time_str[80];
    strftime(_time_str, sizeof(_time_str), "%d-%m-%Y %H:%M:%S", t);

    strcat(logMADE, createdDir);
    pthread_mutex_lock(&lock);
    fprintf(log_file, "%s %s\n", _time_str, logMADE);
    pthread_mutex_unlock(&lock);
}

void writeLogAccess(char folderPath[]){
    // write log
    time_t now = time(NULL);
    struct tm *t = localtime(&now);
    char logAccess[256] = "ACCESSED ";
    char _time_str[80];
    strftime(_time_str, sizeof(_time_str), "%d-%m-%Y %H:%M:%S", t);

    strcat(logAccess, folderPath);
    pthread_mutex_lock(&lock);
    fprintf(log_file, "%s %s\n", _time_str, logAccess);
    pthread_mutex_unlock(&lock);
}


void* create_directory(void* arg) {
    struct ThreadArgs* thread_args = (struct ThreadArgs*) arg;
    char* dirname = thread_args->dirname;
    int maxFile = thread_args->maxFile;
    char ext[4];
    strcpy(ext, dirname);
    char categorized_dirname[MAX_LEN] = "categorized/";
    char new_dirname[MAX_LEN];
    struct stat st;

    //new directory "categorized/"
    strcpy(new_dirname, categorized_dirname);
    strcat(new_dirname, dirname);

    //count file
    char countFile[256];
    char hehe_dir[20] ="files";
    char cmd[256];
    snprintf(cmd, sizeof(cmd), "find %s -type f -iname \"*.%s\" | wc -l", hehe_dir, ext);

    //execute command
    FILE* fp;
    fp = popen(cmd, "r");
    if (fp == NULL) {
        printf("Error: Failed to execute command.\n");
    }

    //read output
    writeLogAccess(hehe_dir);
    fgets(countFile, sizeof(countFile), fp);
    int numFiles = atoi(countFile);

    //ascending output
    char _write[MAX_LEN];
    system("touch buffer.txt");
    sprintf(_write, "echo \"extension_%s : %d\" >> buffer.txt", ext, numFiles);
    system(_write);
    
    //count total directory 
    int numDir;
    if (maxFile == 0){
        numDir = 1;
    } else {
        numDir = numFiles / maxFile ;
        if (numFiles % maxFile > 0)
            numDir = numDir + 1;
    }

    //close stream
    pclose(fp);

    //create dir  if numDir == 0
    if (numDir == 0 && mkdir(new_dirname, 0777) == 0) {
    // directory creation was successful, so do something
        writeLogMade(new_dirname);
    }

    for (int i = 0; i < numDir; i++)
    {
         char _dirName[30];
         strcpy(_dirName, new_dirname);
         if(i > 0) {
            char suffix[10];
            sprintf(suffix, " (%d)", i+1);
            strcat(_dirName, suffix);
        }       

        //check directory
        if (stat(_dirName, &st) == 0) {
            printf("Thread %ld: Direktori '%s' sudah ada.\n", pthread_self(), _dirName);
        } else {
                //make dir if not exist
                if (mkdir(_dirName, 0777) == 0) {
                    writeLogMade(_dirName);
                    //move files according to their extensions
                    char command1[600];
                    char command2[500];
                    char command3[100];
                    char command4[600];
                    char logAccessSource[200];
                    char logAccessTarget[100];

                    sprintf(logAccessSource, "find %s/ -type f -iname \"*.%s\" |xargs -I{} sh -c 'echo \"$(date +\"%%d-%%m-%%Y %%H:%%M:%%S\") ACCESSED $(dirname \"{}\")\"' >> log.txt", hehe_dir, ext);
                    sprintf(logAccessTarget, "echo \"$(date +\"%%d-%%m-%%Y %%H:%%M:%%S\") ACCESSED $(dirname '%s') \">> log.txt", _dirName);
                    
                    system (logAccessSource);
                    system (logAccessTarget);

                    sprintf(command3, "mv \"{}\" \"%s/\"", _dirName);
                    sprintf(command2, "echo \"$(date +\"%%d-%%m-%%Y %%H:%%M:%%S\") MOVED %s file : {} > %s\">> log.txt && %s", ext, _dirName, command3);

                    if (maxFile == 0)
                    {
                        sprintf(command4, "find %s/ -type f -iname \"*.%s\" |xargs -I {} sh -c '%s' ", hehe_dir, ext, command2);
                        system(command4);
                    } else {
                        sprintf(command1, "find %s/ -type f -iname \"*.%s\" |head -%d |xargs -I {} sh -c '%s' ", hehe_dir, ext, maxFile, command2);

                        system(command1);

                    }


                } else {
                    perror("Gagal membuat direktori");
                    
                }
            }
    }
    return NULL;
}


int main() {
    // open log.txt
    log_file = fopen("log.txt", "a");
    if (log_file == NULL) {
        perror("Failed to open log file");
        exit(1);
    }

    // initiate mutex lock
    if (pthread_mutex_init(&lock, NULL) != 0) {
        perror("Failed to initialize mutex lock");
        exit(1);
    }

    char filename[50] = "extensions.txt";
    char dirname[MAX_LEN];
    FILE *fp;
    struct stat st;
    pthread_t tid[MAX_LEN];
    struct ThreadArgs thread_args[MAX_LEN];
    int i = 0;
    
    // make categorized dir if no dir
    if (stat("categorized", &st) != 0) {
        mkdir("categorized", 0777);
        writeLogMade("categorized");
    }

    //read max.txt and save the value
    char maxfilename[50] = "max.txt";
    //open max.txt
    FILE *max;
    //open file
    max = fopen(maxfilename, "r");

    //check if file open
    if (max == NULL) {
        printf("File tidak dapat dibuka.\n");
        exit(1);
    }

    int maxFile;
    fscanf(max, "%d", &maxFile); // read the first line of max.txt into firstLine

     //close max.txt
    fclose(max);

    //open extensions.txt
    fp = fopen(filename, "r");

    //check if extensions.txt open
    if (fp == NULL) {
        printf("File tidak dapat dibuka.\n");
        exit(1);
    }

    //read directory name 
    while (fgets(dirname, MAX_LEN, fp)) {
        //delete newline
        dirname[strcspn(dirname, "\n")] = 0;

        //delete space in the end
        size_t len = strlen(dirname);
        while (len > 0 && isspace(dirname[len - 1])) {
            len--;
        }
        dirname[len] = '\0';

        //make other directory asynchronous using thread
        strcpy(thread_args[i].dirname, dirname);
        thread_args[i].maxFile = maxFile;
        pthread_create(&tid[i], NULL, create_directory, &thread_args[i]);
        i++;
    }

    //join thread
    for (int j = 0; j < i; j++) {
        pthread_join(tid[j], NULL);
    }

    //make dir "other" in "categorized"
    if (stat("categorized/other", &st) != 0) {
        writeLogMade("categorized/other");
        if (mkdir("categorized/other", 0777) != 0)
            printf("Gagal membuat direktori 'other' di dalam 'categorized'.\n");
    }

    // move other files to categorized/other
    char cmdOther1[600];
    char cmdOther2[500];
    char cmdOther3[100];
    char otherPath[20] = "categorized/other";
    char logAccessSource[200];
    char logAccessTarget[100];

    sprintf(logAccessSource, "find files/ -type f |xargs -I{} sh -c 'echo \"$(date +\"%%d-%%m-%%Y %%H:%%M:%%S\") ACCESSED $(dirname \"{}\")\"' >> log.txt");
    sprintf(logAccessTarget, "echo \"$(date +\"%%d-%%m-%%Y %%H:%%M:%%S\") ACCESSED $(dirname '%s') \">> log.txt", otherPath);
    
    system(logAccessSource);
    system(logAccessTarget);

    sprintf(cmdOther3, "mv '\\''{}'\\'' \"%s/\"", otherPath);
    sprintf(cmdOther2, "echo \"$(date +\"%%d-%%m-%%Y %%H:%%M:%%S\") MOVED other file : '\\''{}'\\'' > %s\">> log.txt && %s", otherPath, cmdOther3);
    sprintf(cmdOther1, "find files/ -type f |xargs -I {} sh -c '%s' ", cmdOther2);
    system(cmdOther1);

    //output sort buffer.txt untuk jumlah file tiap extension
    system("sort buffer.txt");
    system("rm -rf buffer.txt");

    // count how many files in dir categorized/other
    DIR *dir;
    struct dirent *ent;
    int count = 0;
    if ((dir = opendir (otherPath)) != NULL) {
        writeLogAccess(otherPath);
        while ((ent = readdir (dir)) != NULL) {
            if(ent->d_type == DT_REG) {
                count++;
            }
        }
        closedir (dir);
    } else {
        perror ("Tidak bisa membuka direktori");
        return 1;
    }
    printf("other : %d\n", count);

    fclose(fp);
    
    pthread_mutex_destroy(&lock);

    fclose(log_file);

    return 0;
}
```
Program kedua adalah categorize.c, program ini secara garis besar akan melakukan pengkategorian pada file-file yang terdapat pada folder hasil unzip di program pertama yaitu ```unzip.c```, berikut adalah penjelasan dari program diatas:

```sh
struct ThreadArgs {
    char dirname[MAX_LEN];
    int maxFile;
};
```
Kode tersebut mendefinisikan sebuah struct bernama ```ThreadArgs``` yang memiliki dua variabel anggota, yaitu ```dirname``` dan ```maxFile```. Variabel dirname merupakan sebuah array karakter dengan ukuran ```MAX_LEN``` yang menyimpan nama direktori yang akan dibuat.

Variabel ```maxFile``` adalah sebuah bilangan bulat yang menunjukkan jumlah file maksimum dalam satu direktori. Struct ini kemudian digunakan sebagai argumen untuk thread pada tahap pembuatan direktori untuk setiap ekstensi.

```sh
void writeLogMade(char createdDir[]){
    // write log
    time_t now = time(NULL);
    struct tm *t = localtime(&now);
    char logMADE[256] = "MADE ";
    char _time_str[80];
    strftime(_time_str, sizeof(_time_str), "%d-%m-%Y %H:%M:%S", t);

    strcat(logMADE, createdDir);
    pthread_mutex_lock(&lock);
    fprintf(log_file, "%s %s\n", _time_str, logMADE);
    pthread_mutex_unlock(&lock);
}
```
Fungsi `writeLogMade` digunakan untuk menulis pesan log ke dalam file log.txt ketika direktori berhasil dibuat oleh program. Pesan log yang ditulis berisi informasi waktu pembuatan direktori dan nama direktori yang dibuat.

Berikut adalah penjelasan detail untuk setiap baris kode di dalam fungsi tersebut:

- `writeLogMade` 

    Menerima sebuah argumen bertipe char yang menyimpan nama direktori yang berhasil dibuat.

- Fungsi ini menggunakan fungsi `time` untuk mendapatkan informasi waktu sistem saat ini, dan menyimpan informasi tersebut ke dalam variabel `now`.

- Selanjutnya, informasi waktu tersebut diubah menjadi string format tanggal dan waktu dengan menggunakan fungsi `localtime` dan `strftime`, dan disimpan ke dalam variabel` _time_str`.

- Variabel `logMADE` didefinisikan sebagai string konstan "MADE ", yang akan digabungkan dengan nama direktori yang dibuat untuk membentuk pesan log.

- `pthread_mutex_lock(&lock)` 

    Mengunci penggunaan mutex sehingga tidak terjadi kesalahan pada saat menulis log.

- `fprintf` 

    Menulis pesan log ke dalam file **log.txt**. Pesan log terdiri dari informasi waktu yang disimpan dalam variabel `_time_str` dan informasi direktori yang dibuat yang disimpan dalam variabel `logMADE`.

- `pthread_mutex_unlock(&lock)`

    Membuka penggunaan mutex yang sebelumnya dikunci sehingga mutex bisa digunakan oleh thread lain.

Fungsi ini dipanggil setiap kali pembuatan folder dilakukan, yaitu setiap terdapat `mkdir`, maka fungsi ini akan dipanggil.

```sh
void writeLogAccess(char folderPath[]){
    // write log
    time_t now = time(NULL);
    struct tm *t = localtime(&now);
    char logAccess[256] = "ACCESSED ";
    char _time_str[80];
    strftime(_time_str, sizeof(_time_str), "%d-%m-%Y %H:%M:%S", t);

    strcat(logAccess, folderPath);
    pthread_mutex_lock(&lock);
    fprintf(log_file, "%s %s\n", _time_str, logAccess);
    pthread_mutex_unlock(&lock);
}
```

Kode di atas adalah definisi dari fungsi `writeLogAccess` yang berfungsi untuk menulis pesan log ke file log. Fungsi ini menerima satu argumen yaitu `folderPath`, yang akan digunakan untuk menuliskan pesan log mengenai akses ke folder tertentu.

Pertama, fungsi ini mendapatkan waktu saat ini menggunakan fungsi `time()` dan `localtime()`, lalu menyimpannya ke dalam variabel `now` dan `t` berturut-turut. Selanjutnya, fungsi ini membuat string `logAccess` yang diawali dengan kata "ACCESSED " dan diikuti dengan `folderPath`.

Kemudian, fungsi ini mengunci mutex dengan menggunakan fungsi `pthread_mutex_lock()`, menuliskan pesan log menggunakan `fprintf()`, dan membuka kunci mutex dengan `pthread_mutex_unlock()`. Pesan log ditulis ke dalam file log dengan format waktu dan pesan log yang telah dibuat sebelumnya.

Fungsi ini dipanggil setiap kali suatu folder dibuka, yaitu saat terdapat perintah `fopen`. 

Selain itu, log ACCESSED juga dibuat saat pemindahan file dilakukan antar dua direktori yang berbeda. Pada kasus ini, maka akan dicatat path log ACCESSED untuk direktori path source dan juga path destination. Untuk melakukan hal ini, kami memanggil log ACCESSED juga ketika dengan log MOVED dilakukan. Pemindahan file dilakukan di dalam fungsi `create_directory` untuk memindahkan file ke categorized/[direktori extention], dan juga dilakukan pemindahan file di dalam main untuk memindahkan file yang masuk ke dalam kategori other ke categorized/other.

```sh
void* create_directory(void* arg) {
    struct ThreadArgs* thread_args = (struct ThreadArgs*) arg;
    char* dirname = thread_args->dirname;
    int maxFile = thread_args->maxFile;
    char ext[4];
    strcpy(ext, dirname);
    char categorized_dirname[MAX_LEN] = "categorized/";
    char new_dirname[MAX_LEN];
    struct stat st;

    //new directory "categorized/"
    strcpy(new_dirname, categorized_dirname);
    strcat(new_dirname, dirname);

    //count file
    char countFile[256];
    char hehe_dir[20] ="files";
    char cmd[256];
    snprintf(cmd, sizeof(cmd), "find %s -type f -iname \"*.%s\" | wc -l", hehe_dir, ext);

    //execute command
    FILE* fp;
    fp = popen(cmd, "r");
    if (fp == NULL) {
        printf("Error: Failed to execute command.\n");
    }

    //read output
    writeLogAccess(hehe_dir);
    fgets(countFile, sizeof(countFile), fp);
    int numFiles = atoi(countFile);

    //ascending output
    char _write[MAX_LEN];
    system("touch buffer.txt");
    sprintf(_write, "echo \"extension_%s : %d\" >> buffer.txt", ext, numFiles);
    system(_write);
    
    //count total directory 
    int numDir;
    if (maxFile == 0){
        numDir = 1;
    } else {
        numDir = numFiles / maxFile ;
        if (numFiles % maxFile > 0)
            numDir = numDir + 1;
    }

    //close stream
    pclose(fp);

    //create dir  if numDir == 0
    if (numDir == 0 && mkdir(new_dirname, 0777) == 0) {
    // directory creation was successful, so do something
        writeLogMade(new_dirname);
    }

    for (int i = 0; i < numDir; i++)
    {
         char _dirName[30];
         strcpy(_dirName, new_dirname);
         if(i > 0) {
            char suffix[10];
            sprintf(suffix, " (%d)", i+1);
            strcat(_dirName, suffix);
        }       

        //check directory
        if (stat(_dirName, &st) == 0) {
            printf("Thread %ld: Direktori '%s' sudah ada.\n", pthread_self(), _dirName);
        } else {
                //make dir if not exist
                if (mkdir(_dirName, 0777) == 0) {
                    writeLogMade(_dirName);
                    //move files according to their extensions
                    char command1[600];
                    char command2[500];
                    char command3[100];
                    char command4[600];
                    char logAccessSource[200];
                    char logAccessTarget[100];

                    sprintf(logAccessSource, "find %s/ -type f -iname \"*.%s\" |xargs -I{} sh -c 'echo \"$(date +\"%%d-%%m-%%Y %%H:%%M:%%S\") ACCESSED $(dirname \"{}\")\"' >> log.txt", hehe_dir, ext);
                    sprintf(logAccessTarget, "echo \"$(date +\"%%d-%%m-%%Y %%H:%%M:%%S\") ACCESSED $(dirname '%s') \">> log.txt", _dirName);
                    
                    system (logAccessSource);
                    system (logAccessTarget);

                    sprintf(command3, "mv \"{}\" \"%s/\"", _dirName);
                    sprintf(command2, "echo \"$(date +\"%%d-%%m-%%Y %%H:%%M:%%S\") MOVED %s file : {} > %s\">> log.txt && %s", ext, _dirName, command3);

                    if (maxFile == 0)
                    {
                        sprintf(command4, "find %s/ -type f -iname \"*.%s\" |xargs -I {} sh -c '%s' ", hehe_dir, ext, command2);
                        system(command4);
                    } else {
                        sprintf(command1, "find %s/ -type f -iname \"*.%s\" |head -%d |xargs -I {} sh -c '%s' ", hehe_dir, ext, maxFile, command2);

                        system(command1);

                    }


                } else {
                    perror("Gagal membuat direktori");
                    
                }
            }
    }
    return NULL;
}
```

Kode tersebut merupakan implementasi dari sebuah fungsi yang bertujuan untuk membuat direktori baru secara dinamis, berdasarkan jenis file ekstensi yang telah ditentukan sebelumnya.

Fungsi ini menerima sebuah argumen berupa struktur data `ThreadArgs` yang berisi informasi tentang nama direktori, jumlah file maksimum yang diperbolehkan dalam satu direktori, serta jumlah file dengan ekstensi yang sesuai dengan nama direktori tersebut.

Langkah-langkah yang dilakukan dalam fungsi ini adalah sebagai berikut:

Membuat nama direktori baru dengan menambahkan prefix "categorized/" pada nama direktori asli.**

```c
    struct ThreadArgs* thread_args = (struct ThreadArgs*) arg;
    char* dirname = thread_args->dirname;
    int maxFile = thread_args->maxFile;
    char ext[4];
    strcpy(ext, dirname);
    char categorized_dirname[MAX_LEN] = "categorized/";
    char new_dirname[MAX_LEN];
    struct stat st;

    // membuat nama direktori baru dengan menambahkan prefix "categorized/"
    strcpy(new_dirname, categorized_dirname);
    strcat(new_dirname, dirname);
```

Kode tersebut merupakan implementasi fungsi `create_directory` yang akan dijalankan oleh thread untuk membuat direktori baru. Fungsi ini menerima satu argumen arg berupa pointer ke struktur `ThreadArgs` yang berisi nama direktori dan nilai `maxFile`.

Pertama, argumen `arg` dikonversi menjadi pointer ke struktur `ThreadArgs` dengan menggunakan casting. Kemudian, nilai nama direktori dan maxFile disimpan dalam variabel `dirname` dan `maxFile`, masing-masing.

Selanjutnya, variabel `ext` dideklarasikan sebagai array karakter dengan ukuran 4 dan diisi dengan nilai `dirname`. Ini dilakukan untuk menyimpan ekstensi file yang akan dikategorikan.

Selanjutnya, variabel `categorized_dirname` dideklarasikan sebagai array karakter dengan ukuran `MAX_LEN` dan diinisialisasi dengan string "categorized/". Kemudian, variabel `new_dirname` dideklarasikan sebagai array karakter dengan ukuran `MAX_LEN`. Nilai `categorized_dirname` ditambahkan ke `new_dirname` menggunakan fungsi `strcpy` dan `strcat`, untuk membuat nama direktori baru dengan menambahkan prefix "categorized/" pada nilai dirname.


Akhirnya, struktur `st` dideklarasikan sebagai variabel untuk menyimpan informasi file/direktori dan fungsi `mkdir` digunakan untuk membuat direktori baru dengan nama `new_dirname`.


Menghitung jumlah file dengan ekstensi yang sesuai dengan nama direktori tersebut.**

```c
    //hitung banyak file
    char countFile[256];
    char hehe_dir[20] ="files";
    char cmd[256];
    snprintf(cmd, sizeof(cmd), "find %s -type f -iname \"*.%s\" | wc -l", hehe_dir, ext);

    // Mengeksekusi command dan membuka stream untuk membaca outputnya
    FILE* fp;
    fp = popen(cmd, "r");
    if (fp == NULL) {
        printf("Error: Failed to execute command.\n");
    }

    // Membaca output dari stream dan menampilkannya ke layar
    writeLogAccess(hehe_dir);
    fgets(countFile, sizeof(countFile), fp);
    int numFiles = atoi(countFile);

    // ascending output
    char _write[MAX_LEN];
    system("touch buffer.txt");
    sprintf(_write, "echo \"extension_%s : %d\" >> buffer.txt", ext, numFiles);
    system(_write);
```
Kode tersebut bertujuan untuk menghitung jumlah file dalam direktori "files" yang memiliki ekstensi tertentu. Secara rinci, berikut adalah penjelasan setiap baris kode:

- `char countFile[256];`

    Deklarasi array karakter untuk menampung output dari command `wc -l`.

- `char hehe_dir[20] ="files";`

    Mendeklarasikan variabel `hehe_dir` sebagai string "files".

- `char cmd[256];`
        
    Mendeklarasikan variabel cmd untuk menampung perintah `find` dan `wc -l`.
    
- `snprintf(cmd, sizeof(cmd), "find %s -type f -iname \"*.%s\" | wc -l", hehe_dir, ext);`
   
    Mengisi variabel cmd dengan perintah `find` dan `wc -l` untuk menghitung jumlah file dalam direktori "files" yang memiliki ekstensi tertentu. `%s` diisi dengan variabel `hehe_dir` dan `%s` yang kedua diisi dengan variabel `ext`.
   
- `FILE* fp;`
   
    Mendeklarasikan variabel `fp` sebagai pointer ke file.
    
- `fp = popen(cmd, "r");`
     
    Mengeksekusi command yang disimpan dalam variabel `cmd` dan membuka stream untuk membaca outputnya.
    
- `if (fp == NULL) {...}`
      
    Mengecek apakah stream berhasil dibuka atau tidak.
    
- `writeLogAccess(hehe_dir);`
      
    Memanggil fungsi `writeLogAccess` untuk mencatat akses ke direktori "files" pada file log.
    
- `fgets(countFile, sizeof(countFile), fp);`
      
    Membaca output dari stream dan menyimpannya dalam variabel `countFile`.
    
- `int numFiles = atoi(countFile);`
       
    Mengkonversi string yang disimpan dalam variabel `countFile` menjadi integer dan menyimpannya dalam variabel `numFiles`.
    
- `char _write[MAX_LEN];`
       
    Mendeklarasikan variabel `_write` sebagai array karakter dengan ukuran `MAX_LEN`.
   
- `system("touch buffer.txt");`
       
    Menjalankan command touch buffer.txt untuk membuat file "buffer.txt" jika belum ada.
   
- `sprintf(_write, "echo \"extension_%s : %d\" >> buffer.txt", ext, numFiles);`
       
    Mengisi variabel `_write` dengan command untuk menambahkan informasi jumlah file ke dalam file "buffer.txt". %s diisi dengan variabel `ext` dan `%d` diisi dengan variabel `numFiles`.
    
- `system(_write);`
      
    Menjalankan command yang disimpan dalam variabel `_write`.

Menghitung jumlah direktori yang dibutuhkan untuk menyimpan seluruh file yang sesuai dengan ekstensi tersebut, berdasarkan batasan jumlah file maksimum dalam satu direktori yang telah ditentukan sebelumnya.**

```c
//menghitung total direktori yang dibuat per ext 
    int numDir;
    if (maxFile == 0){
        numDir = 1;
    } else {
        numDir = numFiles / maxFile ;
        if (numFiles % maxFile > 0)
            numDir = numDir + 1;
    }
// Menutup stream dan mengembalikan nilai 0 (sukses)
    pclose(fp);
```

Kode tersebut adalah untuk menghitung jumlah direktori yang dibuat berdasarkan jumlah file yang ada dalam satu ekstensi tertentu dan batasan maksimum file dalam satu direktori.

Pertama, variabel `numDir` dideklarasikan sebagai variabel yang akan menampung jumlah direktori yang dibuat.

Kemudian, dilakukan pengecekan apakah nilai `maxFile` (batasan maksimum file dalam satu direktori) sama dengan **0**. Jika iya, maka nilai `numDir` di-set menjadi **1** karena hanya akan dibuat satu direktori.

Jika `maxFile` tidak sama dengan 0, maka nilai `numFiles` (jumlah file dalam satu ekstensi tertentu) akan dibagi dengan `maxFile` untuk menghitung berapa banyak direktori yang dibutuhkan. Hasil pembagian ini akan disimpan ke dalam variabel `numDir`.

Selanjutnya, akan dilakukan pengecekan apakah terdapat sisa hasil bagi dari pembagian `numFiles` dengan `maxFile`. Jika ada, maka artinya ada file yang belum terakomodasi di direktori yang sudah dibuat. Oleh karena itu, nilai `numDir` akan **ditambah 1** untuk menampung direktori tambahan yang dibutuhkan.

Fungsi `pclose()` digunakan untuk menutup stream yang dibuka oleh fungsi `popen()`. Setelah stream ditutup, nilai pengembalian dari `pclose()` akan menjadi status akhir dari proses yang dijalankan menggunakan `popen()`. Dalam hal ini, nilai pengembalian 0 menandakan bahwa proses telah berhasil dijalankan dengan baik dan telah selesai. 

Dengan demikian, nilai `numDir` akan menunjukkan **jumlah total direktori** yang dibuat.

Membuat direktori-direktori baru sesuai dengan jumlah yang telah dihitung sebelumnya.**

```c
//create dir  if numDir == 0
    if (numDir == 0 && mkdir(new_dirname, 0777) == 0) {
    // directory creation was successful, so do something
        writeLogMade(new_dirname);
    }
```
Kode tersebut merupakan implementasi dari fungsi untuk membuat direktori baru. Jika variabel `numDir` bernilai 0, maka itu berarti tidak perlu membuat subdirektori baru karena tidak ada file yang akan disimpan di dalamnya. Oleh karena itu, program akan membuat direktori baru dengan nama yang diberikan dalam parameter `new_dirname` menggunakan fungsi `mkdir()` yang merupakan fungsi bawaan dari sistem operasi linux.

Jika proses pembuatan direktori berhasil dilakukan (`mkdir()` mengembalikan nilai 0), maka fungsi `writeLogMade()` akan dipanggil dengan parameter nama direktori baru `(new_dirname)`. Fungsi ini bertugas untuk mencatat log atau riwayat pembuatan direktori dalam sebuah file log.txt yang telah ditentukan sebelumnya.

Memindahkan file-file yang sesuai dengan ekstensi ke dalam direktori-direktori baru yang telah dibuat, dan menulis log berupa informasi tentang file yang telah dipindahkan, waktu pemindahan file, serta direktori tujuan pemindahan file.**

```c
for (int i = 0; i < numDir; i++)
    {
         // mendefinisikan variable nama directory dengan suffix sesuai dengan index
         char _dirName[30];
         strcpy(_dirName, new_dirname);
         if(i > 0) {
            char suffix[10];
            sprintf(suffix, " (%d)", i+1);
            strcat(_dirName, suffix);
        }       

        // memeriksa apakah direktori sudah ada
        if (stat(_dirName, &st) == 0) {
            printf("Thread %ld: Direktori '%s' sudah ada.\n", pthread_self(), _dirName);
        } else {
                // membuat direktori jika belum ada
                if (mkdir(_dirName, 0777) == 0) {
                    writeLogMade(_dirName);
                    //move files according to their extensions
                    char command1[600];
                    char command2[500];
                    char command3[100];
                    char command4[600];
                    char logAccessSource[200];
                    char logAccessTarget[100];

                    sprintf(logAccessSource, "find %s/ -type f -iname \"*.%s\" |xargs -I{} sh -c 'echo \"$(date +\"%%d-%%m-%%Y %%H:%%M:%%S\") ACCESSED $(dirname \"{}\")\"' >> log.txt", hehe_dir, ext);
                    sprintf(logAccessTarget, "echo \"$(date +\"%%d-%%m-%%Y %%H:%%M:%%S\") ACCESSED $(dirname '%s') \">> log.txt", _dirName);
                    
                    system (logAccessSource);
                    system (logAccessTarget);

                    sprintf(command3, "mv \"{}\" \"%s/\"", _dirName);
                    sprintf(command2, "echo \"$(date +\"%%d-%%m-%%Y %%H:%%M:%%S\") MOVED %s file : {} > %s\">> log.txt && %s", ext, _dirName, command3);

                    if (maxFile == 0)
                    {
                        sprintf(command4, "find %s/ -type f -iname \"*.%s\" |xargs -I {} sh -c '%s' ", hehe_dir, ext, command2);
                        system(command4);
                    } else {
                        sprintf(command1, "find %s/ -type f -iname \"*.%s\" |head -%d |xargs -I {} sh -c '%s' ", hehe_dir, ext, maxFile, command2);

                        system(command1);

                    }


                } else {
                    perror("Gagal membuat direktori");
                    
                }
            }
    }
```

Kode ini merupakan bagian dari program untuk memindahkan file-file dengan ekstensi tertentu ke dalam beberapa direktori berdasarkan jumlah maksimal file dalam satu direktori yang ditentukan oleh pengguna. Kode ini akan menjalankan perulangan untuk membuat direktori sebanyak `numDir` kali.

Pertama-tama, kode akan mendefinisikan variable `_dirName` untuk menampung nama direktori dengan tambahan **suffix** (jika` numDir > 1`). Kemudian, kode akan memeriksa apakah direktori sudah ada menggunakan fungsi `stat`. Jika sudah ada, program akan mencetak pesan bahwa direktori sudah ada. Namun, jika belum ada, program akan membuat direktori tersebut menggunakan fungsi `mkdir` dan menulis pesan di file log menggunakan fungsi `writeLogMade`.

Setelah direktori berhasil dibuat, program akan memindahkan file-file dengan ekstensi tertentu dari direktori asal ke direktori tujuan dengan menggunakan perintah `mv`. Selain itu, program juga akan mencatat informasi akses ke direktori pada file **log.txt** menggunakan perintah `find` dan `xargs`.

Jika `maxFile == 0`, program akan memindahkan seluruh file dengan ekstensi tertentu ke direktori yang telah dibuat. Namun, jika `maxFile != 0`, program hanya akan memindahkan sejumlah file sesuai dengan jumlah maksimal file dalam satu direktori yang ditentukan oleh dalam file "max.txt". Jika file-file berhasil dipindahkan maka akan dicatat juga informasi log untuk MOVED ke dalam **log.txt** yang format syntaxnya disimpan dalam variabel `command2`.

Terakhir, jika pembuatan direktori gagal, program akan mencetak pesan kesalahan menggunakan fungsi `perror`.

```sh
int main() {
    // open log.txt
    log_file = fopen("log.txt", "a");
    if (log_file == NULL) {
        perror("Failed to open log file");
        exit(1);
    }

    // initiate mutex lock
    if (pthread_mutex_init(&lock, NULL) != 0) {
        perror("Failed to initialize mutex lock");
        exit(1);
    }

    char filename[50] = "extensions.txt";
    char dirname[MAX_LEN];
    FILE *fp;
    struct stat st;
    pthread_t tid[MAX_LEN];
    struct ThreadArgs thread_args[MAX_LEN];
    int i = 0;
    
    // make categorized dir if no dir
    if (stat("categorized", &st) != 0) {
        mkdir("categorized", 0777);
        writeLogMade("categorized");
    }

    //read max.txt and save the value
    char maxfilename[50] = "max.txt";
    //open max.txt
    FILE *max;
    //open file
    max = fopen(maxfilename, "r");

    //check if file open
    if (max == NULL) {
        printf("File tidak dapat dibuka.\n");
        exit(1);
    }

    int maxFile;
    fscanf(max, "%d", &maxFile); // read the first line of max.txt into firstLine

     //close max.txt
    fclose(max);

    //open extensions.txt
    fp = fopen(filename, "r");

    //check if extensions.txt open
    if (fp == NULL) {
        printf("File tidak dapat dibuka.\n");
        exit(1);
    }

    //read directory name 
    while (fgets(dirname, MAX_LEN, fp)) {
        //delete newline
        dirname[strcspn(dirname, "\n")] = 0;

        //delete space in the end
        size_t len = strlen(dirname);
        while (len > 0 && isspace(dirname[len - 1])) {
            len--;
        }
        dirname[len] = '\0';

        //make other directory asynchronous using thread
        strcpy(thread_args[i].dirname, dirname);
        thread_args[i].maxFile = maxFile;
        pthread_create(&tid[i], NULL, create_directory, &thread_args[i]);
        i++;
    }

    //join thread
    for (int j = 0; j < i; j++) {
        pthread_join(tid[j], NULL);
    }

    //make dir "other" in "categorized"
    if (stat("categorized/other", &st) != 0) {
        writeLogMade("categorized/other");
        if (mkdir("categorized/other", 0777) != 0)
            printf("Gagal membuat direktori 'other' di dalam 'categorized'.\n");
    }

    // move other files to categorized/other
    char cmdOther1[600];
    char cmdOther2[500];
    char cmdOther3[100];
    char otherPath[20] = "categorized/other";
    char logAccessSource[200];
    char logAccessTarget[100];

    sprintf(logAccessSource, "find files/ -type f |xargs -I{} sh -c 'echo \"$(date +\"%%d-%%m-%%Y %%H:%%M:%%S\") ACCESSED $(dirname \"{}\")\"' >> log.txt");
    sprintf(logAccessTarget, "echo \"$(date +\"%%d-%%m-%%Y %%H:%%M:%%S\") ACCESSED $(dirname '%s') \">> log.txt", otherPath);
    
    system(logAccessSource);
    system(logAccessTarget);

    sprintf(cmdOther3, "mv '\\''{}'\\'' \"%s/\"", otherPath);
    sprintf(cmdOther2, "echo \"$(date +\"%%d-%%m-%%Y %%H:%%M:%%S\") MOVED other file : '\\''{}'\\'' > %s\">> log.txt && %s", otherPath, cmdOther3);
    sprintf(cmdOther1, "find files/ -type f |xargs -I {} sh -c '%s' ", cmdOther2);
    system(cmdOther1);

    //output sort buffer.txt untuk jumlah file tiap extension
    system("sort buffer.txt");
    system("rm -rf buffer.txt");

    // count how many files in dir categorized/other
    DIR *dir;
    struct dirent *ent;
    int count = 0;
    if ((dir = opendir (otherPath)) != NULL) {
        writeLogAccess(otherPath);
        while ((ent = readdir (dir)) != NULL) {
            if(ent->d_type == DT_REG) {
                count++;
            }
        }
        closedir (dir);
    } else {
        perror ("Tidak bisa membuka direktori");
        return 1;
    }
    printf("other : %d\n", count);

    fclose(fp);
    
    pthread_mutex_destroy(&lock);

    fclose(log_file);

    return 0;
}
```
Berikut adalah penjelasan dari bagian-bagian pada funsi `main()`:

```sh
// open log.txt
    log_file = fopen("log.txt", "a");
    if (log_file == NULL) {
        perror("Failed to open log file");
        exit(1);
    }

    // initiate mutex lock
    if (pthread_mutex_init(&lock, NULL) != 0) {
        perror("Failed to initialize mutex lock");
        exit(1);
    }
```
Kode tersebut merupakan inisialisasi untuk membuka file log dan menginisialisasi `mutex lock` pada program.

Pada bagian pertama, fungsi `fopen()` digunakan untuk membuka file log.txt dengan mode "a", yaitu mode untuk menambahkan isi file baru pada akhir file yang sudah ada, sehingga log dari setiap proses yang dilakukan akan ditambahkan pada akhir file log.txt yang sudah ada. Jika file tidak berhasil dibuka, maka program akan keluar dan menampilkan pesan error.

Pada bagian kedua, fungsi `pthread_mutex_init()` digunakan untuk menginisialisasi mutex lock pada program. Mutex lock digunakan untuk mengatur akses ke bagian program yang sama agar tidak dilakukan akses secara bersamaan oleh beberapa thread pada saat yang sama. Jika gagal melakukan inisialisasi mutex lock, maka program akan keluar dan menampilkan pesan error.

```sh
char filename[50] = "extensions.txt";
    char dirname[MAX_LEN];
    FILE *fp;
    struct stat st;
    pthread_t tid[MAX_LEN];
    struct ThreadArgs thread_args[MAX_LEN];
    int i = 0;
    
    // make categorized dir if no dir
    if (stat("categorized", &st) != 0) {
        mkdir("categorized", 0777);
        writeLogMade("categorized");
    }

    //read max.txt and save the value
    char maxfilename[50] = "max.txt";
    //open max.txt
    FILE *max;
    //open file
    max = fopen(maxfilename, "r");

    //check if file open
    if (max == NULL) {
        printf("File tidak dapat dibuka.\n");
        exit(1);
    }

    int maxFile;
    fscanf(max, "%d", &maxFile); // read the first line of max.txt into firstLine

     //close max.txt
    fclose(max);

    //open extensions.txt
    fp = fopen(filename, "r");

    //check if extensions.txt open
    if (fp == NULL) {
        printf("File tidak dapat dibuka.\n");
        exit(1);
    }
```

Pada bagian ini akan dilakukan membaca atau mendapatkan ekstensi file apa saja yang akan dikategorikan dengan membaca file `extension.txt`.

Selanjutnya, terdapat bagian yang membuat direktori `categorized` jika direktori tersebut belum ada.

Setelah itu juga terdapat bagian yang akan membuka file `max.txt` dan membaca nilai atau value yang terdapat dalam file tersebut yang akan digunakan sebagai batas maksimal untuk berapa banyak file yang bisa dimuat dalam direktori nantinya.

```sh
//join thread
    for (int j = 0; j < i; j++) {
        pthread_join(tid[j], NULL);
    }
```
Bagian di atas merupakan loop untuk menjalankan fungsi `pthread_join` yang digunakan untuk menggabungkan thread yang masih berjalan ke dalam thread utama.

```sh
//make dir "other" in "categorized"
    if (stat("categorized/other", &st) != 0) {
        writeLogMade("categorized/other");
        if (mkdir("categorized/other", 0777) != 0)
            printf("Gagal membuat direktori 'other' di dalam 'categorized'.\n");
    }
```
Bagian diatas akan membuat sub direktori `other` dalam direktori `categorized`.

```sh
// move other files to categorized/other
    char cmdOther1[600];
    char cmdOther2[500];
    char cmdOther3[100];
    char otherPath[20] = "categorized/other";
    char logAccessSource[200];
    char logAccessTarget[100];

    sprintf(logAccessSource, "find files/ -type f |xargs -I{} sh -c 'echo \"$(date +\"%%d-%%m-%%Y %%H:%%M:%%S\") ACCESSED $(dirname \"{}\")\"' >> log.txt");
    sprintf(logAccessTarget, "echo \"$(date +\"%%d-%%m-%%Y %%H:%%M:%%S\") ACCESSED $(dirname '%s') \">> log.txt", otherPath);
    
    system(logAccessSource);
    system(logAccessTarget);

    sprintf(cmdOther3, "mv '\\''{}'\\'' \"%s/\"", otherPath);
    sprintf(cmdOther2, "echo \"$(date +\"%%d-%%m-%%Y %%H:%%M:%%S\") MOVED other file : '\\''{}'\\'' > %s\">> log.txt && %s", otherPath, cmdOther3);
    sprintf(cmdOther1, "find files/ -type f |xargs -I {} sh -c '%s' ", cmdOther2);
    system(cmdOther1);
```
Bagian tersebut merupakan bagian dari program untuk mengkategorikan file-file yang ada pada direktori "files" berdasarkan ekstensi file tersebut. Kode tersebut bertanggung jawab untuk memindahkan file yang tidak memiliki ekstensi yang terdaftar ke dalam direktori "categorized/other".

```sh
// count how many files in dir categorized/other
    DIR *dir;
    struct dirent *ent;
    int count = 0;
    if ((dir = opendir (otherPath)) != NULL) {
        writeLogAccess(otherPath);
        while ((ent = readdir (dir)) != NULL) {
            if(ent->d_type == DT_REG) {
                count++;
            }
        }
        closedir (dir);
    } else {
        perror ("Tidak bisa membuka direktori");
        return 1;
    }
    printf("other : %d\n", count);
```
Bagian ini bertujuan untuk menghitung jumlah file yang terdapat di dalam direktori "categorized/other".

#### **`logchecker.c`**
```sh
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main() {
    // Accessed count
    printf ("\e[33mTotal banyak akses:\e[0m\n");
    system("grep ACCESSED log.txt | wc -l");

    // List folders & total file
    printf ("\e[33m\nList folder & total file tiap folder:\e[0m\n");
    system("grep 'MOVED\\|MADE' log.txt \
    | awk -F 'categorized' '{print \"categorized\"$NF}' \
    | sed '/^$/d' \
    | sort \
    | uniq -c \
    | awk '{print $2 \" \"$3 \"=\" \" \" $1-1}'");


    //every extension total file (ascending)
    printf ("\n\e[33mTotal file tiap extension\e[0m\n");
    system("grep 'MADE\\|MOVED' log.txt \
    | grep -o 'categorized/[^o]*' \
    | sed 's/categorized\\///' \
    | sed '/^$/d' \
    | sed 's/ \\([^o]*\\)//' \
    | sort \
    | uniq -c \
    | awk '{print $2 \" \" \"=\" \" \" $1-1}'");

    return 0;
}
```
Program C di atas adalah program untuk mengekstrak informasi dari file log.txt yang mencatat aktivitas pada sebuah sistem file.

Pada fungsi main(), terdapat tiga bagian program yang masing-masing memiliki tujuan untuk menghasilkan tiga jenis informasi yang berbeda. Bagian pertama program ini mencetak jumlah akses ke dalam sistem file dengan menggunakan perintah `grep ACCESSED log.txt | wc -l`. Perintah ini akan menghitung berapa kali kata **ACCESSED** muncul di dalam file log.txt dan mengembalikan jumlahnya.

Bagian kedua program ini mencetak daftar folder dan total file di dalam setiap folder. Untuk mendapatkan informasi ini, program ini menggunakan beberapa perintah. Pertama, `grep 'MOVED\|MADE log.txt` akan mencari kata **MOVED** atau **MADE** di dalam file log.txt. Selanjutnya, perintah `awk -F 'categorized' '{print "categorized"$NF}'` akan menambahkan kata "categorized" di depan nama folder untuk setiap baris yang dihasilkan dari perintah sebelumnya. Kemudian, perintah `sed '/^$/d'` akan menghapus baris kosong dari output perintah sebelumnya. Selanjutnya, perintah `sort` dan `uniq -c` akan menghitung jumlah file di setiap folder dan menghapus baris duplikat. Terakhir, perintah `awk '{print $2 " "$3 "=" " " $1-1}'` akan mencetak daftar folder dan jumlah file di dalamnya.

Bagian terakhir program ini mencetak total file di dalam setiap ekstensi file secara terurut menurut urutan abjad. Untuk mendapatkan informasi ini, program ini menggunakan beberapa perintah. Pertama, `grep 'MADE\|MOVED' log.txt` akan mencari kata **MADE** atau **MOVED** di dalam file log.txt. Selanjutnya, perintah `grep -o 'categorized/[^o]'` akan mencari setiap baris yang berisi kata "categorized" diikuti dengan "/" dan diikuti dengan karakter apa pun kecuali "o". Kemudian, perintah `sed 's/categorized\///'` akan menghapus kata "categorized/" dari setiap baris yang dihasilkan dari perintah sebelumnya. Selanjutnya, perintah `sed '/^$/d'` akan menghapus baris kosong dari output perintah sebelumnya. Kemudian, perintah `sed 's/ \([^o]\)//'` akan menghapus setiap karakter setelah spasi dan sebelum karakter "o" dari setiap baris yang dihasilkan dari perintah sebelumnya. Kemudian, perintah `sort` dan `uniq -c` akan menghitung jumlah file untuk setiap ekstensi dan menghapus baris duplikat. Terakhir, perintah `awk '{print $2 " " "=" " " $1-1}'` akan mencetak setiap ekstensi dan jumlah file di dalamnya.

**Untuk melakukan run soal no.4 diperlukan urutan `unzip.c` - `categorize.c` - `logchecker.c` agar berjalan dengan benar.**
### Screenshoot Solusi Soal 4
- Folder files yang merupakan hasil dari unzip file `hehe.zip`
![1](https://github.com/mavaldi/image-placeeee/blob/main/4%20-%201.png?raw=true)
- Folder categorized yang merupakan tempat folder-folder kategori ekstensi
![2](https://github.com/mavaldi/image-placeeee/blob/main/4%20-%202.png?raw=true)
- Salah satu folder ekstensi yaitu emc yang berisi file-file berekstensi `emc` dengan maksimal 10 file 
![3](https://github.com/mavaldi/image-placeeee/blob/main/4%20-%203.png?raw=true)
- Salah satu folder ekstensi yaitu xyz yang tidak berisi apa-apa karena tidak ada satupun file yang memiliki ekstensi `xyz`
![4](https://github.com/mavaldi/image-placeeee/blob/main/4%20-%204.png?raw=true)
- Beberapa line dari isi file `log.txt`
![5](https://github.com/mavaldi/image-placeeee/blob/main/4%20-%205.png?raw=true)
- Output dari program `logchecker.c`
![6](https://github.com/mavaldi/image-placeeee/blob/main/4%20-%206.png?raw=true)

### Kendala Pengerjaan Soal 4
- Pengerjaan soal 4 kami lambat karena pengerjaan no 1, 2, 3 juga selesai mepet jadi no 4 baru bisa dikerjakan pada saat revisi.
