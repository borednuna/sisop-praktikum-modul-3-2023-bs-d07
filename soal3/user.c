#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <pthread.h>
#include <semaphore.h>

#define MAX_MSG_SIZE 1024
#define MAX_USERS 2

typedef struct msg_buffer {
    long msg_type;
    char msg_text[MAX_MSG_SIZE];
} MsgBuffer;

sem_t semaphore;

void send_message() {
    // Send a message to the message queue
    key_t key = ftok("msgq_file", 'A');
    int msg_id = msgget(key, 0666 | IPC_CREAT);
    MsgBuffer msg_buffer;
    msg_buffer.msg_type = 1;

    while (1) {
        fgets(msg_buffer.msg_text, MAX_MSG_SIZE, stdin);
        msgsnd(msg_id, &msg_buffer, sizeof(MsgBuffer), 0);
        if (strcmp(msg_buffer.msg_text, "quit\n") == 0) {
            break;
        }
    }

    printf("Message queue deleted.\n");
}

int main(int argc, char** argv) {
    sem_init(&semaphore, 0, 2); // Initialize the semaphore to the maximum number of users

    while (1) {
        // Attempt to acquire the semaphore
        if (sem_wait(&semaphore) == 0) {
            printf("Semaphore acquired!\n");

            // Send message
            send_message();

            sem_post(&semaphore); // Release the semaphore
            printf("Semaphore released.\n");
        } else {
            char msg[MAX_MSG_SIZE];
            fgets(msg, MAX_MSG_SIZE, stdin);
            printf("STREAM SYSTEM OVERLOAD\n");
        }
    }

    sem_destroy(&semaphore); // Destroy the semaphore
    return 0;
}
