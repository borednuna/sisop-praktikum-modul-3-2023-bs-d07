#include <openssl/bio.h>
#include <openssl/evp.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <ctype.h>
#include <jansson.h>
#include <pthread.h>
#include <semaphore.h>

#define MAX_MSG_SIZE 1024
#define MAX_USERS 2

typedef struct msg_buffer
{
    long msg_type;
    char msg_text[MAX_MSG_SIZE];
} MsgBuffer;

sem_t semaphore;

void* receive_messages(void* arg) {
    // Receive messages from the message queue
    key_t key = ftok("msgq_file", 'A');
    int msg_id = msgget(key, 0666 | IPC_CREAT);
    MsgBuffer msg_buffer;

    while (1) {
        msgrcv(msg_id, &msg_buffer, sizeof(MsgBuffer), 1, 0);
        if (strstr(msg_buffer.msg_text, "quit") != NULL || strstr(msg_buffer.msg_text, "exit") != NULL) {
            break;
        }

        // HANDLE USER COMMAND
        if (strstr(msg_buffer.msg_text, "decrypt") != NULL || strstr(msg_buffer.msg_text, "DECRYPT") != 0) {
            printf("Decrypting...\n");

            // Create new txt file
            system("touch playlist.txt");

            // Clear the txt file content
            FILE *file;
            file = fopen("/home/nuna/sisop-praktikum-modul-3-2023-bs-d07/soal3/playlist.txt", "w");
            fclose(file);

            // Parse JSON file that contains array of objects
            json_t *root;
            json_error_t error;
            root = json_load_file("/home/nuna/sisop-praktikum-modul-3-2023-bs-d07/soal3/song-playlist.json", 0, &error);

            // Handle error
            if (!root) {
                fprintf(stderr, "Error parsing JSON: %s\n", error.text);
                exit(1);
            }

            size_t array_size = json_array_size(root);
            // Loop through array in JSON file
            for (size_t i = 0; i < array_size; i++) {
                json_t *object = json_array_get(root, i);
                if (!json_is_object(object))
                {
                    fprintf(stderr, "Error: JSON array element %zu is not an object\n", i);
                    json_decref(root);
                    exit(1);
                }

                json_t *method = json_object_get(object, "method");
                json_t *song = json_object_get(object, "song");

                char *strmethod = json_string_value(method);
                char *strsong = json_string_value(song);
                char result[1024];

                // Decrypt according to method
                if (strcmp(strmethod, "rot13") == 0)
                {
                    int length = strlen(strsong);
                    int j;
                    for (j = 0; j < length; j++)
                    {
                        if (strsong[j] >= 'a' && strsong[j] <= 'z') {
                            strsong[j] = 'a' + (strsong[j] - 'a' + 13) % 26;
                        } else if (strsong[j] >= 'A' && strsong[j] <= 'Z') {
                            strsong[j] = 'A' + (strsong[j] - 'A' + 13) % 26;
                        }
                    }
                    strcpy(result, strsong);
                } else if (strcmp(strmethod, "base64") == 0) {
                    size_t input_len = strlen(strsong);
                    size_t output_len = input_len * 3 / 4; // approximate output buffer size
                    unsigned char output[output_len];
                    memset(output, 0, output_len);

                    BIO *bio, *b64;
                    b64 = BIO_new(BIO_f_base64());
                    bio = BIO_new_mem_buf(strsong, input_len);
                    bio = BIO_push(b64, bio);
                    BIO_set_flags(bio, BIO_FLAGS_BASE64_NO_NL);
                    output_len = BIO_read(bio, output, input_len);

                    strcpy(result, output);

                    BIO_free_all(bio);
                } else {
                    char hex_str[1024];
                    strcpy(hex_str, strsong);
                    int len = strlen(hex_str) / 2; // Calculate the length of the binary string

                    // Allocate memory for the binary string
                    char *bin_str = (char *)malloc(len + 1);

                    // Convert the hexadecimal string to binary
                    for (int j = 0; j < len; j++) {
                        sscanf(hex_str + 2 * j, "%2hhx", bin_str + j);
                    }

                    // Null-terminate the binary string
                    bin_str[len] = '\0';

                    // Interpret the binary string as a character string
                    printf("Decrypted string: %s\n", bin_str);
                    strcpy(result, bin_str);

                    // Free the memory
                    free(bin_str);
                }

                FILE *file;
                file = fopen("/home/nuna/sisop-praktikum-modul-3-2023-bs-d07/soal3/playlist.txt", "a");

                // Store decrypted song to txt file
                fprintf(file, "%s\n", result);
                fclose(file);
            }

            // Clean JSON resources
            json_decref(root);
            printf("Decrypting done.\n");

            // Sort the lines in txt file alphabetically
            printf("Sorting...\n");
            system("sort -o playlist.txt playlist.txt");
            printf("Sorting done.\n============\n\n");
        } else if (strstr(msg_buffer.msg_text, "list") != NULL || strstr(msg_buffer.msg_text, "LIST") != 0) {
            // Print the content of txt file
            printf("============\n");
            system("cat playlist.txt");
            printf("============\n\n");
        } else if (strstr(msg_buffer.msg_text, "play") != NULL || strstr(msg_buffer.msg_text, "PLAY") != 0) {
            char song[MAX_MSG_SIZE];
            strcpy(song, msg_buffer.msg_text);
            
            // Remove 'play ' from the string
            memmove(song, song + 5, strlen(song));

            // Remove '\n' from the string
            song[strcspn(song, "\n")] = 0;

            // Open playlist file
            FILE *file;
            file = fopen("/home/nuna/sisop-praktikum-modul-3-2023-bs-d07/soal3/playlist.txt", "r");

            // Loop through and find the song and store it in array
            char songs_found[MAX_MSG_SIZE][MAX_MSG_SIZE];
            char line[1024];
            int j = 0;

            while (fgets(line, sizeof(line), file)) {
                // Uppercase letters in line
                for (int i = 0; line[i] != '\0'; i++) {
                    if (line[i] >= 'a' && line[i] <= 'z')
                    {
                        line[i] = line[i] - 32;
                    }
                }

                // Uppercase letters in song
                for (int i = 0; song[i] != '\0'; i++) {
                    if (song[i] >= 'a' && song[i] <= 'z')
                    {
                        song[i] = song[i] - 32;
                    }
                }

                if (strstr(line, song) != NULL) {
                    strcpy(songs_found[j], line);
                    j++;
                }
            }

            int sem_id = semget(key, 1, 0666 | IPC_CREAT);

            // Play the song
            if (j == 1) {
                // Remove newline character
                songs_found[0][strcspn(songs_found[0], "\n")] = 0;
                printf("USER %d PLAYING \"%s\"\n", sem_id, songs_found[0]);
                printf("============\n\n");
            } else if (j > 1) {
                printf("THERE ARE \"%d\" SONG CONTAINING \"%s\"\n", j, song);
                int k = 0;
                while (k < j) {
                    printf("%d. %s", k+1, songs_found[k]);
                    k++;
                }
            } else {
                printf("THERE IS NO SONG CONTAINING %s\n", song);
            }

            fclose(file);
            printf("===========================\n");
        } else if (strstr(msg_buffer.msg_text, "add") != NULL || strstr(msg_buffer.msg_text, "ADD") != 0) {
            // Get song name
            char song[MAX_MSG_SIZE];
            strcpy(song, msg_buffer.msg_text);

            // Remove 'add ' from the string
            memmove(song, song + 4, strlen(song));

            // Remove trailing newline
            song[strcspn(song, "\n")] = 0;

            // Check if song already exist in file
            int already_exist = 0;
            FILE *file;
            file = fopen("/home/nuna/sisop-praktikum-modul-3-2023-bs-d07/soal3/playlist.txt", "r");
            while(fgets(msg_buffer.msg_text, sizeof(msg_buffer.msg_text), file) != NULL) {
                if (strstr(msg_buffer.msg_text, song) != NULL) {
                    printf("SONG ALREADY ON PLAYLIST\n");
                    already_exist = 1;
                }
            }

            if (!already_exist) {
                // Open playlist file in append mode
                FILE *file;
                file = fopen("/home/nuna/sisop-praktikum-modul-3-2023-bs-d07/soal3/playlist.txt", "a");

                // Write song into playlist file
                fprintf(file, "%s\n", song);
                fclose(file);

                // Sort the lines in txt file alphabetically
                system("sort -o playlist.txt playlist.txt");
                int sem_id = semget(key, 1, 0666 | IPC_CREAT);
                printf("USER %d ADD SONG \"%s\"\n", sem_id, song);
            }
        } else {
            printf("UNKNOWN COMMAND\n");
        }
    }

    msgctl(msg_id, IPC_RMID, NULL);
    printf("strsong queue deleted.\n");
    return NULL;
}

int main() {
    pthread_t thread_id;
    sem_init(&semaphore, 0, MAX_USERS); // Initialize the semaphore to the maximum number of users

    pthread_create(&thread_id, NULL, receive_messages, NULL);

    // Wait for the thread to finish
    pthread_join(thread_id, NULL);

    sem_destroy(&semaphore); // Destroy the semaphore

    return 0;
}
