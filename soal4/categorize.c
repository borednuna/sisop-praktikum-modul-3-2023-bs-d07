#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>
#include <pthread.h>
#include <ctype.h>
#include <math.h> 

#define MAX_LEN 256
#define MAX_PATH 256
#define MAX_EXT 5
#define MAX_DEST_SIZE 475 //buffer max

pthread_mutex_t lock;
FILE* log_file;

struct ThreadArgs {
    char dirname[MAX_LEN];
    int maxFile;
};

void writeLogMade(char createdDir[]){
    // write log
    time_t now = time(NULL);
    struct tm *t = localtime(&now);
    char logMADE[256] = "MADE ";
    char _time_str[80];
    strftime(_time_str, sizeof(_time_str), "%d-%m-%Y %H:%M:%S", t);

    strcat(logMADE, createdDir);
    pthread_mutex_lock(&lock);
    fprintf(log_file, "%s %s\n", _time_str, logMADE);
    pthread_mutex_unlock(&lock);
}

void writeLogAccess(char folderPath[]){
    // write log
    time_t now = time(NULL);
    struct tm *t = localtime(&now);
    char logAccess[256] = "ACCESSED ";
    char _time_str[80];
    strftime(_time_str, sizeof(_time_str), "%d-%m-%Y %H:%M:%S", t);

    strcat(logAccess, folderPath);
    pthread_mutex_lock(&lock);
    fprintf(log_file, "%s %s\n", _time_str, logAccess);
    pthread_mutex_unlock(&lock);
}


void* create_directory(void* arg) {
    struct ThreadArgs* thread_args = (struct ThreadArgs*) arg;
    char* dirname = thread_args->dirname;
    int maxFile = thread_args->maxFile;
    char ext[4];
    strcpy(ext, dirname);
    char categorized_dirname[MAX_LEN] = "categorized/";
    char new_dirname[MAX_LEN];
    struct stat st;

    //new directory "categorized/"
    strcpy(new_dirname, categorized_dirname);
    strcat(new_dirname, dirname);

    //count file
    char countFile[256];
    char hehe_dir[20] ="files";
    char cmd[256];
    snprintf(cmd, sizeof(cmd), "find %s -type f -iname \"*.%s\" | wc -l", hehe_dir, ext);

    //execute command
    FILE* fp;
    fp = popen(cmd, "r");
    if (fp == NULL) {
        printf("Error: Failed to execute command.\n");
    }

    //read output
    writeLogAccess(hehe_dir);
    fgets(countFile, sizeof(countFile), fp);
    int numFiles = atoi(countFile);

    //ascending output
    char _write[MAX_LEN];
    system("touch buffer.txt");
    sprintf(_write, "echo \"extension_%s : %d\" >> buffer.txt", ext, numFiles);
    system(_write);
    
    //count total directory 
    int numDir;
    if (maxFile == 0){
        numDir = 1;
    } else {
        numDir = numFiles / maxFile ;
        if (numFiles % maxFile > 0)
            numDir = numDir + 1;
    }

    //close stream
    pclose(fp);

    //create dir  if numDir == 0
    if (numDir == 0 && mkdir(new_dirname, 0777) == 0) {
    // directory creation was successful, so do something
        writeLogMade(new_dirname);
    }

    for (int i = 0; i < numDir; i++)
    {
         char _dirName[30];
         strcpy(_dirName, new_dirname);
         if(i > 0) {
            char suffix[10];
            sprintf(suffix, " (%d)", i+1);
            strcat(_dirName, suffix);
        }       

        //check directory
        if (stat(_dirName, &st) == 0) {
            printf("Thread %ld: Direktori '%s' sudah ada.\n", pthread_self(), _dirName);
        } else {
                //make dir if not exist
                if (mkdir(_dirName, 0777) == 0) {
                    writeLogMade(_dirName);
                    //move files according to their extensions
                    char command1[600];
                    char command2[500];
                    char command3[100];
                    char command4[600];
                    char logAccessSource[200];
                    char logAccessTarget[100];

                    sprintf(logAccessSource, "find %s/ -type f -iname \"*.%s\" |xargs -I{} sh -c 'echo \"$(date +\"%%d-%%m-%%Y %%H:%%M:%%S\") ACCESSED $(dirname \"{}\")\"' >> log.txt", hehe_dir, ext);
                    sprintf(logAccessTarget, "echo \"$(date +\"%%d-%%m-%%Y %%H:%%M:%%S\") ACCESSED $(dirname '%s') \">> log.txt", _dirName);
                    
                    system (logAccessSource);
                    system (logAccessTarget);

                    sprintf(command3, "mv \"{}\" \"%s/\"", _dirName);
                    sprintf(command2, "echo \"$(date +\"%%d-%%m-%%Y %%H:%%M:%%S\") MOVED %s file : {} > %s\">> log.txt && %s", ext, _dirName, command3);

                    if (maxFile == 0)
                    {
                        sprintf(command4, "find %s/ -type f -iname \"*.%s\" |xargs -I {} sh -c '%s' ", hehe_dir, ext, command2);
                        system(command4);
                    } else {
                        sprintf(command1, "find %s/ -type f -iname \"*.%s\" |head -%d |xargs -I {} sh -c '%s' ", hehe_dir, ext, maxFile, command2);

                        system(command1);

                    }


                } else {
                    perror("Gagal membuat direktori");
                    
                }
            }
    }
    return NULL;
}


int main() {
    // open log.txt
    log_file = fopen("log.txt", "a");
    if (log_file == NULL) {
        perror("Failed to open log file");
        exit(1);
    }

    // initiate mutex lock
    if (pthread_mutex_init(&lock, NULL) != 0) {
        perror("Failed to initialize mutex lock");
        exit(1);
    }

    char filename[50] = "extensions.txt";
    char dirname[MAX_LEN];
    FILE *fp;
    struct stat st;
    pthread_t tid[MAX_LEN];
    struct ThreadArgs thread_args[MAX_LEN];
    int i = 0;
    
    // make categorized dir if no dir
    if (stat("categorized", &st) != 0) {
        mkdir("categorized", 0777);
        writeLogMade("categorized");
    }

    //read max.txt and save the value
    char maxfilename[50] = "max.txt";
    //open max.txt
    FILE *max;
    //open file
    max = fopen(maxfilename, "r");

    //check if file open
    if (max == NULL) {
        printf("File tidak dapat dibuka.\n");
        exit(1);
    }

    int maxFile;
    fscanf(max, "%d", &maxFile); // read the first line of max.txt into firstLine

     //close max.txt
    fclose(max);

    //open extensions.txt
    fp = fopen(filename, "r");

    //check if extensions.txt open
    if (fp == NULL) {
        printf("File tidak dapat dibuka.\n");
        exit(1);
    }

    //read directory name 
    while (fgets(dirname, MAX_LEN, fp)) {
        //delete newline
        dirname[strcspn(dirname, "\n")] = 0;

        //delete space in the end
        size_t len = strlen(dirname);
        while (len > 0 && isspace(dirname[len - 1])) {
            len--;
        }
        dirname[len] = '\0';

        //make other directory asynchronous using thread
        strcpy(thread_args[i].dirname, dirname);
        thread_args[i].maxFile = maxFile;
        pthread_create(&tid[i], NULL, create_directory, &thread_args[i]);
        i++;
    }

    //join thread
    for (int j = 0; j < i; j++) {
        pthread_join(tid[j], NULL);
    }

    //make dir "other" in "categorized"
    if (stat("categorized/other", &st) != 0) {
        writeLogMade("categorized/other");
        if (mkdir("categorized/other", 0777) != 0)
            printf("Gagal membuat direktori 'other' di dalam 'categorized'.\n");
    }

    // move other files to categorized/other
    char cmdOther1[600];
    char cmdOther2[500];
    char cmdOther3[100];
    char otherPath[20] = "categorized/other";
    char logAccessSource[200];
    char logAccessTarget[100];

    sprintf(logAccessSource, "find files/ -type f |xargs -I{} sh -c 'echo \"$(date +\"%%d-%%m-%%Y %%H:%%M:%%S\") ACCESSED $(dirname \"{}\")\"' >> log.txt");
    sprintf(logAccessTarget, "echo \"$(date +\"%%d-%%m-%%Y %%H:%%M:%%S\") ACCESSED $(dirname '%s') \">> log.txt", otherPath);
    
    system(logAccessSource);
    system(logAccessTarget);

    sprintf(cmdOther3, "mv '\\''{}'\\'' \"%s/\"", otherPath);
    sprintf(cmdOther2, "echo \"$(date +\"%%d-%%m-%%Y %%H:%%M:%%S\") MOVED other file : '\\''{}'\\'' > %s\">> log.txt && %s", otherPath, cmdOther3);
    sprintf(cmdOther1, "find files/ -type f |xargs -I {} sh -c '%s' ", cmdOther2);
    system(cmdOther1);

    //output sort buffer.txt untuk jumlah file tiap extension
    system("sort buffer.txt");
    system("rm -rf buffer.txt");

    // count how many files in dir categorized/other
    DIR *dir;
    struct dirent *ent;
    int count = 0;
    if ((dir = opendir (otherPath)) != NULL) {
        writeLogAccess(otherPath);
        while ((ent = readdir (dir)) != NULL) {
            if(ent->d_type == DT_REG) {
                count++;
            }
        }
        closedir (dir);
    } else {
        perror ("Tidak bisa membuka direktori");
        return 1;
    }
    printf("other : %d\n", count);

    fclose(fp);
    
    pthread_mutex_destroy(&lock);

    fclose(log_file);

    return 0;
}