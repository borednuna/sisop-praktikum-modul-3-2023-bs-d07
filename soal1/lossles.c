#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <string.h>
#include <ctype.h>

#define MAX_CHAR 256
#define MAX_TREE_HT 50

char send[26];

struct MinHeapNode
{
   char data;             // Karakter pada file
   unsigned freq;         // Frekuensi karakter
   struct MinHeapNode* left;
   struct MinHeapNode* right;
};

struct MinHeap {
  unsigned size;
  unsigned capacity;
  struct MinHeapNode **array;
};

struct MinHeapNode *newNode(char data, unsigned freq) {
  struct MinHeapNode *temp = (struct MinHeapNode *)malloc(sizeof(struct MinHeapNode));

  temp->left = temp->right = NULL;
  temp->data = data;
  temp->freq = freq;

  return temp;
}

struct MinHeap *createMinH(unsigned capacity) {
  struct MinHeap *minHeap = (struct MinHeap *)malloc(sizeof(struct MinHeap));

  minHeap->size = 0;

  minHeap->capacity = capacity;

  minHeap->array = (struct MinHeapNode **)malloc(minHeap->capacity * sizeof(struct MinHeapNode *));
  return minHeap;
}

void swapMinHeapNode(struct MinHeapNode **a, struct MinHeapNode **b) {
  struct MinHeapNode *t = *a;
  *a = *b;
  *b = t;
}

void minHeapify(struct MinHeap *minHeap, int idx) {
  int smallest = idx;
  int left = 2 * idx + 1;
  int right = 2 * idx + 2;

  if (left < minHeap->size && minHeap->array[left]->freq < minHeap->array[smallest]->freq)
    smallest = left;

  if (right < minHeap->size && minHeap->array[right]->freq < minHeap->array[smallest]->freq)
    smallest = right;

  if (smallest != idx) {
    swapMinHeapNode(&minHeap->array[smallest], &minHeap->array[idx]);
    minHeapify(minHeap, smallest);
  }
}

int checkSizeOne(struct MinHeap *minHeap) {
  return (minHeap->size == 1);
}

struct MinHeapNode *extractMin(struct MinHeap *minHeap) {
  struct MinHeapNode *temp = minHeap->array[0];
  minHeap->array[0] = minHeap->array[minHeap->size - 1];

  --minHeap->size;
  minHeapify(minHeap, 0);

  return temp;
}

void insertMinHeap(struct MinHeap *minHeap, struct MinHeapNode *minHeapNode) {
  ++minHeap->size;
  int i = minHeap->size - 1;

  while (i && minHeapNode->freq < minHeap->array[(i - 1) / 2]->freq) {
    minHeap->array[i] = minHeap->array[(i - 1) / 2];
    i = (i - 1) / 2;
  }
  minHeap->array[i] = minHeapNode;
}

void buildMinHeap(struct MinHeap *minHeap) {
  int n = minHeap->size - 1;
  int i;

  for (i = (n - 1) / 2; i >= 0; --i)
    minHeapify(minHeap, i);
}

int isLeaf(struct MinHeapNode *root) {
  return !(root->left) && !(root->right);
}

struct MinHeap *createAndBuildMinHeap(char data[], int freq[], int size) {
  struct MinHeap *minHeap = createMinH(size);

  for (int i = 0; i < size; ++i)
    minHeap->array[i] = newNode(data[i], freq[i]);

  minHeap->size = size;
  buildMinHeap(minHeap);

  return minHeap;
}

struct MinHeapNode *buildHuffmanTree(char data[], int freq[], int size) {
  struct MinHeapNode *left, *right, *top;
  struct MinHeap *minHeap = createAndBuildMinHeap(data, freq, size);

  while (!checkSizeOne(minHeap)) {
    left = extractMin(minHeap);
    right = extractMin(minHeap);

    top = newNode('$', left->freq + right->freq);

    top->left = left;
    top->right = right;

    insertMinHeap(minHeap, top);
  }
  return extractMin(minHeap);
}

void printArray(int arr[], int n, char c) {
    FILE *fp = fopen("temp.txt", "a");

    if (fp == NULL) {
        printf("Error opening file\n");
        return;
    }

  int i;

  fprintf(fp, "%c", c);
  for (i = 0; i < n; ++i){
    fprintf(fp, "%d", arr[i]);
  }

  fprintf(fp, "%c", '\n');
  fclose(fp);
}

void printHCodes(struct MinHeapNode *root, int arr[], int top) {
  if (root->left) {
    arr[top] = 0;
    printHCodes(root->left, arr, top + 1);
  }
  if (root->right) {
    arr[top] = 1;
    printHCodes(root->right, arr, top + 1);
  }
  if (isLeaf(root) && strchr(send, root->data)!=NULL) {
    printArray(arr, top, root->data);
  }
}

void HuffmanCodes(char data[], int freq[], int size) {
  struct MinHeapNode *root = buildHuffmanTree(data, freq, size);

  int arr[MAX_TREE_HT], top = 0;

  printHCodes(root, arr, top);
}

int count_line(char filename[]){
    int count = 0;
    char line[512];
    FILE *fp = fopen(filename, "r");

    if (fp == NULL) {
        printf("Error opening file.\n");
        return 1;
    }

    while (fgets(line, 512, fp) != NULL) {
        count++;
    }

    fclose(fp);

    return count;
}

int findLength(char c){
    char line[512];
    FILE *fp = fopen("temp.txt", "r");

    if (fp == NULL) {
        printf("Error opening file.\n");
        return 1;
    }

    while (fgets(line, 512, fp) != NULL) {
    if (strchr(line, c) != NULL) {
        return strlen(line) - 1;
        }
    }

    fclose(fp);
    return -1;
}

int translate(char filename[]) {
    FILE *fp = fopen(filename, "a");
    FILE *fs = fopen("temp.txt", "r");
    FILE *fc = fopen("file.txt", "r");

    if (fp == NULL || fs == NULL || fc == NULL) {
        printf("Error opening file.\n");
        return 1;
    }

    char line[512];
    char c;
    int count = 0;
    while ((c = fgetc(fc)) != EOF) {
        fseek(fs, 0, SEEK_SET);
        c = toupper(c);
        while (fgets(line, 512, fs) != NULL) {
            if (line[0] == c) {
                char *sub = malloc(strlen(line));
                strcpy(sub, line + 1);
                count += strlen(sub)-1;
                sub[strlen(sub) - 1] = '\0';
                fwrite(sub, strlen(sub), 1, fp);
                free(sub);
                break;
            }
        }
    }


    fclose(fp);
    fclose(fs);
    fclose(fc);
    return count;
}

int main() {
    int fd1[2], fd2[2];
    pid_t pid;

    if (pipe(fd1) == -1 || pipe(fd2) == -1) {
        perror("pipe");
        exit(EXIT_FAILURE);
    }

    pid = fork();

    if (pid == -1) {
        perror("fork");
        exit(EXIT_FAILURE);
    }

    //child
    if (pid == 0) { 
        char *arr = malloc(sizeof(char) * 26);
        int *arrint = malloc(sizeof(int) * 26);
        char *sent = malloc(sizeof(char)*26);
        close(fd1[1]);
        read(fd1[0], arr, sizeof(char) * 26);
        read(fd1[0], arrint, sizeof(int) * 26);
        read(fd1[0], sent, 26);
        close(fd1[0]);

        strcpy(send, sent);

        close(fd1[0]);
        HuffmanCodes(arr, arrint, sizeof(char)*26);

        close(fd2[0]);
        int countLine = count_line("temp.txt");
        write(fd2[1], &countLine, sizeof(countLine));
        FILE *fp = fopen("temp.txt", "r");
            if(fp==NULL){
                perror("fail to open");
                exit(1);
            }

            char line[512];
            while(fgets(line, 512, fp)!= NULL){
                write(fd2[1], &line, sizeof(line));
            }
        close(fd2[1]);

        // parent proc
    } else { 
        FILE *fp;
        char filename[100];
        char c;

        fp = fopen("file.txt", "r");
        if (fp == NULL) {
            printf("Error opening file\n");
            return 1;
        }

        int freq[26] = {0}; 
        char alpha[26] = {'\0'}; 
        int idx = 0; 
        while ((c = fgetc(fp)) != EOF) {
            if (isalpha(c)) { 
                c = toupper(c); 
                int pos = c - 'A'; 
                freq[pos]++; 
                if (freq[pos] == 1) { 
                    alpha[idx] = c; 
                    idx++; 
                }
            }
        }

        char *arr = malloc(sizeof(char) * 26);
        int *arrfr = malloc(sizeof(int) * 26);

        int j = 0;
        for (int i = 0; i < 26; i++) {
            alpha[i] = 'A' + i;
            arrfr[i] = freq[i];

            if(freq[i]!=0){
                send[j] = alpha[i];
                j++;
            }
        }

        for (int i = 0; i < 26; i++) {
            arr[i] = alpha[i];
        }

        fclose(fp);

        close(fd1[0]);
        write(fd1[1], arr, sizeof(char) * 26);
        write(fd1[1], arrfr, sizeof(int) * 26);
        write(fd1[1], send, sizeof(send));
        close(fd1[1]);

        close(fd2[1]);
        int n;
        read(fd2[0], &n, sizeof(int));

        char coded[n][512];
        for (int i = 0; i < n; i++) {
            read(fd2[0], coded[i], sizeof(char) * 512);
        }

        close(fd2[0]);
        int number = 0;
        for(int i = 0; i<26; i++){
            int calc;
            if(freq[i]!=0){
                calc = freq[i]*8;
                number += calc;
            }
        }

        printf("Jumlah bit SEBELUM dilakukan kompresi adalah %d\n", number);

        number = translate("translated.txt");;
        printf("Jumlah bit SETELAH dilakukan kompresi adalah %d\n", number);

        system("rm temp.txt");
    }

    return 0;
}
