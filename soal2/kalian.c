#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/ipc.h>
#include <sys/shm.h>

#define ROW1 4
#define COL1 2
#define ROW2 2
#define COL2 5

int main() {
    srand(time(NULL));

    int matrix1[ROW1][COL1];
    int matrix2[ROW2][COL2];
    int result[ROW1][COL2];

    // Fill matrix 1 with random numbers from 1 to 5
    for (int i = 0; i < ROW1; i++) {
        for (int j = 0; j < COL1; j++) {
            matrix1[i][j] = rand() % 5 + 1;
        }
    }
    
    printf("Matrix 1:\n");
    for (int i = 0; i < ROW1; i++) {
        for (int j = 0; j < COL1; j++) {
            printf("%d ", matrix1[i][j]);
        }
        printf("\n");
    }

    // Fill matrix 2 with random numbers from 1 to 4
    for (int i = 0; i < ROW2; i++) {
        for (int j = 0; j < COL2; j++) {
            matrix2[i][j] = rand() % 4 + 1;
        }
    }
    
    printf("\nMatrix 2:\n");
    for (int i = 0; i < ROW2; i++) {
        for (int j = 0; j < COL2; j++) {
            printf("%d ", matrix2[i][j]);
        }
        printf("\n");
    }

    // Matrix multiplication
    for (int i = 0; i < ROW1; i++) {
        for (int j = 0; j < COL2; j++) {
            result[i][j] = 0;
            for (int k = 0; k < COL1; k++) {
                result[i][j] += matrix1[i][k] * matrix2[k][j];
            }
        }
    }
    
    printf("\nResult:\n");
    for (int i = 0; i < ROW1; i++) {
        for (int j = 0; j < COL2; j++) {
            printf("%d ", result[i][j]);
        }
        printf("\n");
    }

    // Create shared memory
    key_t key = ftok("shmfile", 65);
    int shmid = shmget(key, sizeof(result), 0666 | IPC_CREAT);
    int (*shared_result)[COL2] = shmat(shmid, NULL, 0);

    // Copy the result to shared memory
    for (int i = 0; i < ROW1; i++) {
        for (int j = 0; j < COL2; j++) {
            shared_result[i][j] = result[i][j];
        }
    }

    // Detach shared memory
    shmdt(shared_result);

    return 0;
}
