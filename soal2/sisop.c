#include <stdio.h>
#include <sys/ipc.h>
#include <sys/shm.h>
//#include <time.h>

#define ROWS 4
#define COLS 5

unsigned long long factorial(int n) {
    unsigned long long fact = 1;
    for (int i = 1; i <= n; i++) {
        fact *= i;
    }
    return fact;
}

int main() {

    /*
    clock_t start, end;
    start = clock();
    */
    
    // Access shared memory
    key_t key = ftok("shmfile", 65);
    int shmid = shmget(key, sizeof(int[ROWS][COLS]), 0666 | IPC_CREAT);
    int (*result)[COLS] = shmat(shmid, NULL, 0);

    // Print the result matrix
    printf("Result matrix:\n");
    for (int i = 0; i < ROWS; i++) {
        for (int j = 0; j < COLS; j++) {
            printf("%d ", result[i][j]);
        }
        printf("\n");
    }

    // Calculate factorials and store them in a matrix
    unsigned long long factorial_matrix[ROWS][COLS];
    for (int i = 0; i < ROWS; i++) {
        for (int j = 0; j < COLS; j++) {
            factorial_matrix[i][j] = factorial(result[i][j]);
        }
    }

    // Print the factorial matrix
    printf("\nFactorial matrix:\n");
    for (int i = 0; i < ROWS; i++) {
        for (int j = 0; j < COLS; j++) {
            printf("%llu ", factorial_matrix[i][j]);
        }
        printf("\n");
    }

    // Detach and destroy shared memory
    shmdt(result);
    shmctl(shmid, IPC_RMID, NULL);
    
    /*
    end = clock();
    double duration = ((double)end - start)/CLOCKS_PER_SEC;
    printf("Time taken to execute in seconds : %f \n", duration);
    */

    return 0;
}
