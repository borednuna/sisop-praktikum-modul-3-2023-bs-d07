#include <stdio.h>
#include <pthread.h>
#include <sys/ipc.h>
#include <sys/shm.h>
//#include <time.h>

#define ROWS 4
#define COLS 5

typedef struct {
    int row;
    int col;
    int value;
} MatrixElement;

unsigned long long factorial_matrix[ROWS][COLS];

void *factorial(void *arg) {
    MatrixElement *element = (MatrixElement *)arg;
    unsigned long long fact = 1;

    for (int i = 1; i <= element->value; i++) {
        fact *= i;
    }

    factorial_matrix[element->row][element->col] = fact;
    return NULL;
}

int main() {

    /*
    clock_t start, end;
    start = clock();
    */
    
    // Access shared memory
    key_t key = ftok("shmfile", 65);
    int shmid = shmget(key, sizeof(int[ROWS][COLS]), 0666 | IPC_CREAT);
    int (*result)[COLS] = shmat(shmid, NULL, 0);

    // Print the result matrix
    printf("Result matrix:\n");
    for (int i = 0; i < ROWS; i++) {
        for (int j = 0; j < COLS; j++) {
            printf("%d ", result[i][j]);
        }
        printf("\n");
    }

    // Calculate factorials using multithreading
    pthread_t threads[ROWS][COLS];
    MatrixElement elements[ROWS][COLS];

    for (int i = 0; i < ROWS; i++) {
        for (int j = 0; j < COLS; j++) {
            elements[i][j].row = i;
            elements[i][j].col = j;
            elements[i][j].value = result[i][j];
            pthread_create(&threads[i][j], NULL, factorial, &elements[i][j]);
        }
    }

    for (int i = 0; i < ROWS; i++) {
        for (int j = 0; j < COLS; j++) {
            pthread_join(threads[i][j], NULL);
        }
    }

    // Print the factorial matrix
    printf("\nFactorial matrix:\n");
    for (int i = 0; i < ROWS; i++) {
        for (int j = 0; j < COLS; j++) {
            printf("%llu ", factorial_matrix[i][j]);
        }
        printf("\n");
    }

    // Detach and destroy shared memory
    shmdt(result);
    shmctl(shmid, IPC_RMID, NULL);
    
    /*
    end = clock();
    double duration = ((double)end - start)/CLOCKS_PER_SEC;
    printf("Time taken to execute in seconds : %f \n", duration);
    */

    return 0;
}
